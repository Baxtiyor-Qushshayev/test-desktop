package uz.test.desktop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.test.desktop.domain.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {


}
