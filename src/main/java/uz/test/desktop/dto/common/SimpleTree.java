package uz.test.desktop.dto.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SimpleTree implements Serializable {

    private Long id;
    private String label;
    private String labelRu;
    private String code;
    private List<SimpleTreeItem> children;

    public SimpleTree() {
    }

    public SimpleTree(Long id, String label, String labelRu, String code, List<SimpleTreeItem> children) {
        this.id = id;
        this.label = label;
        this.labelRu = labelRu;
        this.code = code;
        this.children = children;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabelRu() {
        return labelRu;
    }

    public void setLabelRu(String labelRu) {
        this.labelRu = labelRu;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<SimpleTreeItem> getChildren() {
        return children;
    }

    public void setChildren(List<SimpleTreeItem> children) {
        this.children = children;
    }
}
