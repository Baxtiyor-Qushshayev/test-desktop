package uz.test.desktop.service;


import uz.test.desktop.domain.QuestionsEntity;
import uz.test.desktop.dto.HideQuestions;
import uz.test.desktop.dto.QuestionsDto;
import uz.test.desktop.dto.common.FilterData;
import uz.test.desktop.dto.common.SimpleTreeItem;

import java.util.List;

public interface QuestionsService {
    void save(List<QuestionsDto> questionsDtos);
    List<QuestionsEntity> findByDepartmentId(Long departmentId);
    List<SimpleTreeItem> getTreeItemList(FilterData filter,boolean isLangChange);
    List<QuestionsDto> findByDepartmentCode(String depCode,int limit);
    List<QuestionsDto> findBySelectedQuestion(List<Long> ids);
    List<QuestionsDto> findByHiddenQuestions();
    void updateByHiddenQuestions(List<HideQuestions> list);
    void updateByIdAndHiddenQuestions(Long id, Boolean aBoolean);
}
