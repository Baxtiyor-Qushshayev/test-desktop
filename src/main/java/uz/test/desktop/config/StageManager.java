package uz.test.desktop.config;

import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uz.test.desktop.view.FxmlView;

import java.util.Objects;
import java.util.ResourceBundle;

public class StageManager {
    private static final Logger LOG = LoggerFactory.getLogger(StageManager.class);
    private final Stage primaryStage;
    private final SpringFXMLLoader springFxmlLoader;

    public StageManager(SpringFXMLLoader springFxmlLoader, Stage stage) {
        this.springFxmlLoader = springFxmlLoader;
        this.primaryStage = stage;
    }

    public void switchScene(final FxmlView view, ResourceBundle bundle) {
        Parent viewRootNodeHierarchy = loadViewNodeHierarchy(view.getFxmlFile(),bundle);
        show(viewRootNodeHierarchy,view.getTitle());
    }

    private void show(Parent rootNode, String title) {
        Scene scene = prepareScene(rootNode);
        scene.getStylesheets().add("/style/styles.css");
        primaryStage.setTitle(title);
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        primaryStage.centerOnScreen();

        try {
            primaryStage.show();
        } catch (Exception e) {
            logAndExit ("Unable to show scene for title" + title,e);
        }

    }

    private void logAndExit(String errorMsg, Exception exception) {
        LOG.error(errorMsg, exception, exception.getCause());
        Platform.exit();
    }

    private Scene prepareScene(Parent rootNode) {
        Scene scene = primaryStage.getScene();

        if (scene == null) {
            scene = new Scene(rootNode);
        }
        scene.setRoot(rootNode);
        return scene;
    }

    private Parent loadViewNodeHierarchy(String fxmlFilePath, ResourceBundle bundle) {
        Parent rootNode = null;
        try {
            rootNode = springFxmlLoader.load(fxmlFilePath,bundle);
            Objects.requireNonNull(rootNode, "A Root FXML node must not be null");
        } catch (Exception exception) {
            logAndExit("Unable to load FXML view" + fxmlFilePath, exception);
        }
        return rootNode;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
}
