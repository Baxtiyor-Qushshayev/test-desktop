package uz.test.desktop.controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import uz.test.desktop.config.StageManager;
import uz.test.desktop.dto.CheckDto;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

@Controller
public class CommentController implements Initializable {

    @FXML
    private BorderPane borderPane;

    @FXML
    private JFXButton btnBack;

    @FXML
    private JFXButton btnNext;

    @FXML
    private JFXButton btnHome;

    @Lazy
    @Autowired
    private StageManager stageManager;

    private TestActionController actionController;

    private ResourceBundle resourceBundle;

    private List<CheckDto> questions;
    private CheckDto commentDto;
    private int k;

    public CommentController(TestActionController actionController) {
        this.actionController = actionController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle = resources;
        questions = new ArrayList<>();
        k = 0;
        questions.addAll(actionController.getResultMistakeQuestions());
        if (!questions.isEmpty()) {
            createCommentNode();
        }
    }

    private void createCommentNode() {
        commentDto = new CheckDto();
        commentDto = questions.get(k);

        final VBox vb = new VBox();
        vb.setPadding(new Insets(10, 15, 10, 15));
        vb.setSpacing(10);

        vb.getChildren().add(new Comment(resourceBundle.getString("CommentController.question.name") + " " + commentDto.getLabel()));
        vb.getChildren().add(new Comment(resourceBundle.getString("CommentController.answers.text") + " "));

        int i = 0;
        String correctAnswer = "";
        int j = 0;
        for (CheckDto item : commentDto.getItems()) {
            Label label = new Label();
            label.setText(" " + (i + 1) + ". " + item.getLabel());
            label.setFont(Font.font(22D));
            label.setWrapText(true);

            vb.getChildren().add(label);

            if (item.getParent() != null && item.getParent().isSelected()) {
                correctAnswer = item.getParent().getLabel();
                j = i + 1;
            }

            i++;
        }

        Label lblCorrectAnswer = new Comment(resourceBundle.getString("CommentController.correctAnswer.text") + " " + j + ". " + correctAnswer);
        lblCorrectAnswer.setPadding(new Insets(10, 0, 10, 0));
        lblCorrectAnswer.setStyle("-fx-text-fill: #28B463");

        vb.getChildren().add(lblCorrectAnswer);

        Label lblComment = new Label();
        lblComment.setStyle("-fx-text-fill: #232323;");
        lblComment.setText(" " + commentDto.getComment());
        lblComment.setFont(Font.font(22D));
        lblComment.setWrapText(true);

        vb.getChildren().add(lblComment);

        Node node = this.borderPane.getCenter();
        if (node != null) {
            this.borderPane.setCenter(null);
        }

        this.borderPane.setCenter(vb);
    }

    @FXML
    void onBtnBack(ActionEvent event) {
        if (k > 0) {
            k--;
            this.createCommentNode();
        }
    }

    @FXML
    void onBtnNext(ActionEvent event) {
        if (k < questions.size() - 1) {
            k++;
            this.createCommentNode();
        }
    }

    @FXML
    void onBtnHome(ActionEvent event) {

    }


    static class Comment extends Label {
        Comment(String text) {
            super(text);
            setWrapText(true);
            setFont(Font.font("System", FontWeight.BOLD, 22D));
        }
    }
}
