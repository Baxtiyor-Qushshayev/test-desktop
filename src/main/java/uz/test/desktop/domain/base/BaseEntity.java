package uz.test.desktop.domain.base;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getId() == null) {
            return false;
        }
        if(!obj.getClass().getName().equals(getClass().getName())){
            return false;
        }
        if (getId().equals(((BaseEntity) obj).getId())) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "[id=" + getId() + "]";
    }

}