package uz.test.desktop.constants;

public enum DialogResponse {
    NO,
    YES,
    CLOSE,
    CANCEL;
}
