package uz.test.desktop.dto;

import uz.test.desktop.constants.Category;

import java.io.Serializable;
import java.util.List;

public class DepartmentCategoryDto implements Serializable {

    private Category category;
    private List<String> departmentCodes;

    public DepartmentCategoryDto() {
    }

    public DepartmentCategoryDto(Category category, List<String> departmentCodes) {
        this.category = category;
        this.departmentCodes = departmentCodes;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<String> getDepartmentCodes() {
        return departmentCodes;
    }

    public void setDepartmentCodes(List<String> departmentCodes) {
        this.departmentCodes = departmentCodes;
    }

    @Override
    public String toString() {
        return "DepartmentCategoryDto{" +
                "category=" + category +
                ", departmentCodes=" + departmentCodes +
                '}';
    }
}
