package uz.test.desktop.service;

import uz.test.desktop.domain.AnswersEntity;

import java.util.List;

public interface AnswersService {

    List<AnswersEntity> findByQuestionId(Long questionId);
}
