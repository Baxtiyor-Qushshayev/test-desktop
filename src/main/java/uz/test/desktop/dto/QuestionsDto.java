package uz.test.desktop.dto;


import uz.test.desktop.dto.base.BaseDto;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class QuestionsDto extends BaseDto {

    private String testNumbers;
    private String questionUz;
    private String questionRu;
    private String commentUz;
    private String commentRu;
    private String partUz;
    private String partRu;
    private String chapterUz;
    private String chapterRu;
    private List<AnswersDto> answers;
    private String department;
    private Long departmentId;
    private boolean isHide = false;

    public String getTestNumbers() {
        return testNumbers;
    }

    public void setTestNumbers(String testNumbers) {
        this.testNumbers = testNumbers;
    }

    public String getQuestionUz() {
        return questionUz;
    }

    public void setQuestionUz(String questionUz) {
        this.questionUz = questionUz;
    }

    public String getQuestionRu() {
        return questionRu;
    }

    public void setQuestionRu(String questionRu) {
        this.questionRu = questionRu;
    }

    public String getCommentUz() {
        return commentUz;
    }

    public void setCommentUz(String commentUz) {
        this.commentUz = commentUz;
    }

    public String getCommentRu() {
        return commentRu;
    }

    public void setCommentRu(String commentRu) {
        this.commentRu = commentRu;
    }

    public String getPartUz() {
        return partUz;
    }

    public void setPartUz(String partUz) {
        this.partUz = partUz;
    }

    public String getPartRu() {
        return partRu;
    }

    public void setPartRu(String partRu) {
        this.partRu = partRu;
    }

    public String getChapterUz() {
        return chapterUz;
    }

    public void setChapterUz(String chapterUz) {
        this.chapterUz = chapterUz;
    }

    public String getChapterRu() {
        return chapterRu;
    }

    public void setChapterRu(String chapterRu) {
        this.chapterRu = chapterRu;
    }

    public List<AnswersDto> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswersDto> answers) {
        this.answers = answers;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public boolean isHide() {
        return isHide;
    }

    public void setHide(boolean hide) {
        isHide = hide;
    }

    public CheckDto asCheckDto(Locale locale) {
        CheckDto dto = new CheckDto();
        dto.setId(getId());
        dto.setLabel(getQuestionUz());
        dto.setComment(getCommentUz());

        if (locale.getLanguage().equalsIgnoreCase("ru")) {
            dto.setLabel(getQuestionRu());
            dto.setComment(getCommentRu());
        }
        dto.setItems(getAnswers().stream().map(answersDto -> answersDto.asCheckItem(locale)).collect(Collectors.toList()));

        return dto;
    }
}
