package uz.test.desktop.dto.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import uz.test.desktop.util.AppUtil;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FilterData implements Serializable {

    private String search;
    private Boolean lookUp;
    private Long ignoreId;

    public FilterData() {
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }


    public boolean isSearchValid() {
        return !AppUtil.isEmpty(getSearch());
    }

    public String searchKey() {
        return AppUtil.searchKey(getSearch());
    }

    public Boolean getLookUp() {
        return lookUp;
    }

    public void setLookUp(Boolean lookUp) {
        this.lookUp = lookUp;
    }

    public Long getIgnoreId() {
        return ignoreId;
    }

    public void setIgnoreId(Long ignoreId) {
        this.ignoreId = ignoreId;
    }
}
