package uz.test.desktop.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultDto implements Serializable {

    private Long total;
    private Long answer;
    private Long mistake;
    private String totalLabel;
    private String answerLabel;
    private String mistakeLabel;

    public ResultDto() {
    }

    public ResultDto(Long total, Long answer, Long mistake, String totalLabel, String answerLabel, String mistakeLabel) {
        this.total = total;
        this.answer = answer;
        this.mistake = mistake;
        this.totalLabel = totalLabel;
        this.answerLabel = answerLabel;
        this.mistakeLabel = mistakeLabel;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getAnswer() {
        return answer;
    }

    public void setAnswer(Long answer) {
        this.answer = answer;
    }

    public Long getMistake() {
        return mistake;
    }

    public void setMistake(Long mistake) {
        this.mistake = mistake;
    }

    public String getTotalLabel() {
        return totalLabel;
    }

    public void setTotalLabel(String totalLabel) {
        this.totalLabel = totalLabel;
    }

    public String getAnswerLabel() {
        return answerLabel;
    }

    public void setAnswerLabel(String answerLabel) {
        this.answerLabel = answerLabel;
    }

    public String getMistakeLabel() {
        return mistakeLabel;
    }

    public void setMistakeLabel(String mistakeLabel) {
        this.mistakeLabel = mistakeLabel;
    }
}
