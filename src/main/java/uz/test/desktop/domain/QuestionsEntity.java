package uz.test.desktop.domain;


import org.hibernate.annotations.Type;
import uz.test.desktop.domain.base.BaseEntity;
import uz.test.desktop.dto.QuestionsDto;
import uz.test.desktop.dto.common.SimpleTreeItem;

import javax.persistence.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "questions")
public class QuestionsEntity extends BaseEntity {

    @Column(name = "questionUz")
    @Type(type = "text")
    private String questionUz;

    @Column(name = "questionRu")
    @Type(type = "text")
    private String questionRu;

    @Column(name = "commentUz")
    @Type(type = "text")
    private String commentUz;

    @Column(name = "commentRu")
    @Type(type = "text")
    private String commentRu;

    @Column(name = "partUz")
    private String partUz;

    @Column(name = "partRu")
    private String partRu;

    @Column(name = "chapterUz")
    private String chapterUz;

    @Column(name = "chapterRu")
    private String chapterRu;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "questionId",cascade = {CascadeType.ALL}, orphanRemoval = true)
    @OrderBy("id desc ")
    private List<AnswersEntity> answers = new LinkedList<>();

    @Column(name = "departmentId")
    private Long departmentId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "departmentId", insertable = false, updatable = false)
    private DepartmentEntity department;

    @Column(name = "code")
    private String code;

    @Column(name="isHide", columnDefinition = "boolean default false")
    private Boolean isHide = Boolean.FALSE;


    public String getQuestionUz() {
        return questionUz;
    }

    public void setQuestionUz(String questionUz) {
        this.questionUz = questionUz;
    }

    public String getQuestionRu() {
        return questionRu;
    }

    public void setQuestionRu(String questionRu) {
        this.questionRu = questionRu;
    }

    public String getCommentUz() {
        return commentUz;
    }

    public void setCommentUz(String commentUz) {
        this.commentUz = commentUz;
    }

    public String getCommentRu() {
        return commentRu;
    }

    public void setCommentRu(String commentRu) {
        this.commentRu = commentRu;
    }

    public String getPartUz() {
        return partUz;
    }

    public void setPartUz(String partUz) {
        this.partUz = partUz;
    }

    public String getPartRu() {
        return partRu;
    }

    public void setPartRu(String partRu) {
        this.partRu = partRu;
    }

    public String getChapterUz() {
        return chapterUz;
    }

    public void setChapterUz(String chapterUz) {
        this.chapterUz = chapterUz;
    }

    public String getChapterRu() {
        return chapterRu;
    }

    public void setChapterRu(String chapterRu) {
        this.chapterRu = chapterRu;
    }

    public List<AnswersEntity> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswersEntity> answers) {
        this.answers = answers;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getHide() {
        return isHide;
    }

    public void setHide(Boolean hide) {
        isHide = hide;
    }

    public SimpleTreeItem asTreeItemDto() {
        return new SimpleTreeItem(getId(),getQuestionUz(),getQuestionRu(),getCode(),(getDepartment()!= null ? getDepartment().asTreeItemDto(): null));
    }

    public QuestionsDto asDto() {
        QuestionsDto dto = new QuestionsDto();
        dto.setId(getId());
        dto.setQuestionUz(getQuestionUz());
        dto.setQuestionRu(getQuestionRu());
        dto.setCommentUz(getCommentUz());
        dto.setCommentRu(getCommentRu());
        dto.setChapterUz(getChapterUz());
        dto.setChapterRu(getChapterRu());
        dto.setPartUz(getPartUz());
        dto.setPartRu(getPartRu());
        dto.setHide(getHide());

        List<AnswersEntity> answersEntityList = new ArrayList<>();
        List<AnswersEntity> copy = new ArrayList<>(getAnswers());

        SecureRandom rand = new SecureRandom();
        for (int i = 0; i < getAnswers().size(); i++) {
            answersEntityList.add(copy.remove(rand.nextInt(copy.size())));
        }

        dto.setAnswers(answersEntityList.stream().map(AnswersEntity::asDto).collect(Collectors.toList()));
        return dto;
    }

    public QuestionsDto asQuestionsDto() {
        QuestionsDto dto = new QuestionsDto();
        dto.setId(getId());
        dto.setQuestionUz(getQuestionUz());
        dto.setQuestionRu(getQuestionRu());
        dto.setCommentUz(getCommentUz());
        dto.setCommentRu(getCommentRu());
        dto.setChapterUz(getChapterUz());
        dto.setChapterRu(getChapterRu());
        dto.setPartUz(getPartUz());
        dto.setPartRu(getPartRu());
        dto.setHide(getHide());
        return dto;
    }
}
