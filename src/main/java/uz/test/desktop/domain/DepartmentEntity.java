package uz.test.desktop.domain;

import org.hibernate.annotations.Type;
import uz.test.desktop.domain.base.BaseEntity;
import uz.test.desktop.dto.DepartmentDto;
import uz.test.desktop.dto.common.SimpleTree;
import uz.test.desktop.dto.common.SimpleTreeItem;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "department")
public class DepartmentEntity extends BaseEntity {

    @Column(name = "nameUz")
    @Type(type = "text")
    private String nameUz;

    @Column(name = "nameRu")
    @Type(type = "text")
    private String nameRu;

    @Column(name = "code")
    private String code;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "departmentId",cascade = {CascadeType.ALL}, orphanRemoval = true)
    @OrderBy("id desc ")
    private List<QuestionsEntity> questions = new LinkedList<>();

    public String getNameUz() {
        return nameUz;
    }

    public void setNameUz(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<QuestionsEntity> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionsEntity> questions) {
        this.questions = questions;
    }

    public DepartmentDto asDto() {
        DepartmentDto dto = new DepartmentDto();
        dto.setId(getId());
        dto.setCode(getCode());
        dto.setNameUz(getNameUz());
        dto.setNameRu(getNameRu());

        return dto;
    }

    public SimpleTree asTreeDto() {
        return new SimpleTree(getId(),getNameUz(),getNameRu(),getCode(),getQuestions().stream().map(QuestionsEntity::asTreeItemDto).collect(Collectors.toList()));
    }

    public SimpleTreeItem asTreeItemDto() {
        return new SimpleTreeItem(getId(),getNameUz(),getNameRu(),getCode());
    }
}
