package uz.test.desktop.constants;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Baxtiyor
 * created on 2021-02-20
 */

public enum ProgressColor {
    RED_BAR("red-bar"),
    YELLOW_BAR("yellow-bar"),
    ORANGE_BAR("orange-bar"),
    GREEN_BAR("green-bar");

    private final String color;

    ProgressColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public static List<String> asColors() {
        return Arrays.stream(values()).map(ProgressColor::getColor).collect(Collectors.toList());
    }
}
