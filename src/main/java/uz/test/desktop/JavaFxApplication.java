package uz.test.desktop;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import uz.test.desktop.config.StageManager;
import uz.test.desktop.view.FxmlView;

import java.util.Locale;
import java.util.ResourceBundle;

public class JavaFxApplication extends Application {
    private ConfigurableApplicationContext applicationContext;
    private StageManager stageManager;

    @Override
    public void init() {
        String[] args = getParameters().getRaw().toArray(new String[0]);

        this.applicationContext = new SpringApplicationBuilder()
                .sources(SpringBootExampleApplication.class)
                .run(args);
    }

    @Override
    public void start(Stage stage) {
        stageManager = applicationContext.getBean(StageManager.class, stage);
        displayInitialScene();
    }

    private void displayInitialScene() {
        stageManager.switchScene(FxmlView.CONTROLS, ResourceBundle.getBundle("messages/messages",new Locale("uz","UZ")));
    }

    @Override
    public void stop() {
        this.applicationContext.close();
        Platform.exit();
    }
}
