package uz.test.desktop.dto.base;

import java.io.Serializable;


public abstract class BaseDto implements Serializable {
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



}
