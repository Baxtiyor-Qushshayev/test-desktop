package uz.test.desktop.dto;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class HideQuestions {

    private final BooleanProperty ishide;
    private final StringProperty id;
    private final StringProperty name;

    public HideQuestions(boolean international, String id, String name) {
        this.ishide = new SimpleBooleanProperty(this, "ishide", international);
        this.id = new SimpleStringProperty(this, "id", id);
        this.name = new SimpleStringProperty(this, "name", name);
    }

    public String getId() {
        return id.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public StringProperty idProperty() {
        return id;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public boolean isIshidel() {
        return ishide.get();
    }

    public void setIshide(boolean international) {
        this.ishide.set(international);
    }

    public BooleanProperty ishideProperty() {
        return ishide;
    }

    @Override
    public String toString() {
        return id.get() + " " + name.get() + (ishide.get() ? " (ishide)" : "");
    }

}
