package uz.test.desktop.config;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ResourceBundle;

@Component
public class SpringFXMLLoader {
    private final ApplicationContext context;

    @Autowired
    public SpringFXMLLoader(ApplicationContext applicationContext) {
        this.context = applicationContext;
    }

    public Parent load(String fxmlPath, ResourceBundle bundle) {
        FXMLLoader loader = new FXMLLoader();
        loader.setControllerFactory(context::getBean);
        loader.setResources(bundle);
        loader.setLocation(getClass().getResource(fxmlPath));

        try {
           return loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
