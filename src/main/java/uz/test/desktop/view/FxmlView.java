package uz.test.desktop.view;

import java.util.ResourceBundle;

public enum FxmlView {

    CONTROLS {
        @Override
        public String getTitle() {
            return getStringFromResourceBundle("window.title");
        }

        @Override
        public String getFxmlFile() {
            return "/controls-view.fxml";
        }
    },
    SETTINGS {
        @Override
        public String getTitle() {
            return getStringFromResourceBundle("SettingsController.window.title");
        }

        @Override
        public String getFxmlFile() {
            return "/settings-view.fxml";
        }
    },
    ACTION {
        @Override
        public String getTitle() {
            return getStringFromResourceBundle("TestActionController.window.title");
        }

        @Override
        public String getFxmlFile() {
            return "/action-test-view2.fxml";
        }
    },
    COMMENT {
        @Override
        public String getTitle() {
            return getStringFromResourceBundle("TestActionController.window.title");
        }

        @Override
        public String getFxmlFile() {
            return "/comment-view.fxml";
        }
    };

    public abstract String getTitle();
    public abstract String getFxmlFile();

    String getStringFromResourceBundle(String key){
        return ResourceBundle.getBundle("messages/messages").getString(key);
    }
}
