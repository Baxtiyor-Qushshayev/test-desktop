package uz.test.desktop.service;

import uz.test.desktop.domain.DepartmentEntity;
import uz.test.desktop.dto.DepartmentDto;
import uz.test.desktop.dto.common.SimpleTree;

import java.util.List;

public interface DepartmentService {
    List<DepartmentEntity> findAllBySortCode();
    List<DepartmentDto> getList();
    List<SimpleTree> getTreeList();
}
