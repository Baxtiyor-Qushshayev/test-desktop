//package uz.test.desktop.controller;
//
//import com.jfoenix.controls.JFXButton;
//import com.jfoenix.controls.JFXTextField;
//import javafx.application.Platform;
//import javafx.beans.property.*;
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.event.ActionEvent;
//import javafx.fxml.FXML;
//import javafx.fxml.Initializable;
//import javafx.geometry.Insets;
//import javafx.scene.Cursor;
//import javafx.scene.control.*;
//import javafx.scene.control.cell.CheckBoxListCell;
//import javafx.scene.layout.*;
//import javafx.util.StringConverter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.stereotype.Controller;
//import uz.test.desktop.config.StageManager;
//import uz.test.desktop.constants.DepartmentCategory;
//import uz.test.desktop.domain.DepartmentEntity;
//import uz.test.desktop.dto.DepartmentDto;
//import uz.test.desktop.constants.Category;
//import uz.test.desktop.dto.DepartmentCategoryDto;
//import uz.test.desktop.service.DepartmentService;
//import uz.test.desktop.view.FxmlView;
//
//import java.net.URL;
//import java.util.*;
//import java.util.stream.Collectors;
//
//@Controller
//public class SettingsController implements Initializable {
//
//    @FXML
//    private JFXButton btnExit;
//
//    @FXML
//    private Label testSectionsLabel;
//
//    @FXML
//    private RadioButton rbSection;
//
//    @FXML
//    private RadioButton rbMixed;
//
//    @FXML
//    private ToggleGroup tgSection;
//
//    @FXML
//    private VBox vbSection;
//
//    @FXML
//    private ListView<Task> listViewSection;
//
//    @FXML
//    private JFXButton btnStartTest;
//
//    @FXML
//    private JFXTextField lblTestNumber;
//
//    @FXML
//    private JFXTextField lblSearch;
//
//    @FXML
//    private JFXButton contact;
//
//    @FXML
//    private JFXButton goal;
//
//
//    @Autowired
//    private DepartmentService departmentService;
//
//    @Lazy
//    @Autowired
//    private StageManager stageManager;
//
//    private ResourceBundle resourceBundle;
//
//    private List<String> departmentCodes = new ArrayList<>();
//    private DepartmentCategoryDto departmentCategory;
//
//    @Override
//    public void initialize(URL location, ResourceBundle resources) {
//        resourceBundle = resources;
//        lblTestNumber.setStyle("");
//        loadDepartments();
//    }
//
//    private void loadDepartments() {
//       /* List<DepartmentEntity> departmentEntities = departmentService.findAll();
//        if (departmentEntities != null && departmentEntities.size() > 0) {
//            List<DepartmentDto> sections = departmentEntities.stream().map(DepartmentEntity::asDto).collect(Collectors.toList());
//
//            Map<String, Long> departmentMapUz = sections.stream().collect(Collectors.toMap(DepartmentDto::getNameUz, DepartmentDto::getId));
//            Map<String, Long> departmentMapRu = sections.stream().collect(Collectors.toMap(DepartmentDto::getNameRu, DepartmentDto::getId));
//
//            List<String> listUz = departmentEntities.stream().map(DepartmentEntity::getNameUz).collect(Collectors.toList());
//            List<String> listRu = departmentEntities.stream().map(DepartmentEntity::getNameRu).collect(Collectors.toList());
//
//            Locale locale = resourceBundle.getLocale();
//
//            if (locale.getLanguage().equalsIgnoreCase("ru")) {
//                createDepartmentList(listRu, departmentMapRu);
//            } else createDepartmentList(listUz, departmentMapUz);
//        } */
//    }
//
//    private void createDepartmentList(List<String> list, Map<String, Long> departmentMap) {
//        rbMixed.setSelected(true);
//        rbMixed.setPadding(new Insets(10));
//        rbMixed.setCursor(Cursor.HAND);
//
//        rbSection.setPadding(new Insets(10));
//        rbSection.setCursor(Cursor.HAND);
//
//        btnStartTest.setPadding(new Insets(10));
//        ObservableList<Task> listSections = FXCollections.observableArrayList(list.stream().map(Task::new).collect(Collectors.toList()));
//        listViewSection.getItems().addAll(listSections);
//        listViewSection.setPrefHeight(list.size() * vbSection.getPrefHeight());
//        listViewSection.setPrefWidth(list.size() * vbSection.getPrefWidth());
//
//       /* listSections.forEach(section -> section.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
//            if (isNowSelected) {
//                System.out.println(departmentMap.get(section.getName()));
//                departmentIds.add(departmentMap.get(section.getName()));
//
//            } else departmentIds.remove(departmentMap.get(section.getName()));
//            System.out.println(isNowSelected);
//        })); */
//
//        listViewSection.setCellFactory(CheckBoxListCell.forListView(Task::selectedProperty, new StringConverter<Task>() {
//            @Override
//            public String toString(Task object) {
//                return object.getName();
//            }
//
//            @Override
//            public Task fromString(String string) {
//                return null;
//            }
//        }));
//    }
//
//    @FXML
//    void onBtnExit(ActionEvent event) {
//        Platform.exit();
//    }
//
//    public static class Task {
//        private ReadOnlyStringWrapper name = new ReadOnlyStringWrapper();
//        private BooleanProperty selected = new SimpleBooleanProperty(false);
//
//        public Task(String name) {
//            this.name.set(name);
//        }
//
//        public String getName() {
//            return name.get();
//        }
//
//        public ReadOnlyStringProperty nameProperty() {
//            return name.getReadOnlyProperty();
//        }
//
//        public BooleanProperty selectedProperty() {
//            return selected;
//        }
//
//        public boolean isSelected() {
//            return selected.get();
//        }
//
//        public void setSelected(boolean selected) {
//            this.selected.set(selected);
//        }
//    }
//
//    @FXML
//    void onBtnStartTest(ActionEvent event) {
//        departmentCategory = new DepartmentCategoryDto();
//        departmentCategory.setCategory(Category.MIXED);
//
//        if (rbSection.isSelected()) {
//            departmentCategory.setCategory(Category.SECTION);
//        }
//        departmentCategory.setDepartmentCodes(departmentCodes);
//
//        System.out.println(departmentCategory);
//        switchWindow(resourceBundle);
//    }
//
//    private void switchWindow(ResourceBundle bundle) {
//        stageManager.switchScene(FxmlView.ACTION, bundle);
//    }
//
//    public DepartmentCategoryDto getDepartmentMapIds() {
//        return departmentCategory;
//    }
//}
