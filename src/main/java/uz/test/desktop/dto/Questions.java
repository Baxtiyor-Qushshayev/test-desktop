package uz.test.desktop.dto;

import javafx.scene.control.CheckBox;

public class Questions {

    private CheckBox check;
    private Long id;
    private String name;


    public CheckBox getCheck() {
        return check;
    }

    public void setCheck(Boolean x) {
        if(x) {
            check.setSelected(true);
        } else {
            check.setSelected(false);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Questions(Long id, String name) {
        check = new CheckBox();
        this.id = id;
        this.name = name;
    }

}
