package uz.test.desktop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.test.desktop.domain.QuestionsEntity;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface QuestionsRepository extends JpaRepository<QuestionsEntity, Long> {


    @Query(value = "SELECT * FROM QUESTIONS Q " +
            "where Q.DEPARTMENT_ID=:departmentId AND Q.IS_HIDE IS TRUE " +
            "ORDER BY RAND() DESC LIMIT :limit ", nativeQuery = true)
    List<QuestionsEntity> getByDepartmentIdList(@Param("departmentId") Long departmentId, @Param("limit") int limit);

    @Query(value = "SELECT q FROM QuestionsEntity q WHERE q.questionUz LIKE %:search%")
    List<QuestionsEntity> findByNameFilter(@Param("search") String search);

    @Query(value = "SELECT q FROM QuestionsEntity q WHERE q.questionRu LIKE %:search%")
    List<QuestionsEntity> findByNameFilterRu(@Param("search") String search);

    @Query(value = "select q from QuestionsEntity q where q.department.code= :depCode and q.isHide is FALSE")
    List<QuestionsEntity> findByDepartmentCode(@Param("depCode") String depCode);

    @Query(value = "select q from QuestionsEntity q where q.id IN :ids ")
    List<QuestionsEntity> findBySelectedQuestionId(@Param("ids") List<Long> ids);

    @Query(value = "SELECT * FROM QUESTIONS Q " +
            "where Q.IS_HIDE IS TRUE " +
            "ORDER BY ID DESC ", nativeQuery = true)
    List<QuestionsEntity> findByHiddenQuestions();



}
