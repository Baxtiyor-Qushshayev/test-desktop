package uz.test.desktop.dto;

import uz.test.desktop.dto.base.BaseDto;

import java.util.List;

public class CheckDto extends BaseDto {

    private String label;
    private String comment;
    private boolean selected;
    private CheckDto parent;
    private List<CheckDto> items;

    public CheckDto() {
    }

    public CheckDto(Long id, String label, boolean selected) {
        this.id = id;
        this.label = label;
        this.selected = selected;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public List<CheckDto> getItems() {
        return items;
    }

    public void setItems(List<CheckDto> items) {
        this.items = items;
    }

    public CheckDto getParent() {
        return parent;
    }

    public void setParent(CheckDto parent) {
        this.parent = parent;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "CheckDto{" +
                "label='" + label + '\'' +
                ", selected=" + selected +
                ", items=" + items +
                ", id=" + id +
                '}';
    }
}
