package uz.test.desktop.dto;


import uz.test.desktop.dto.base.BaseDto;

import java.util.Locale;

public class AnswersDto  extends BaseDto {

    private String answerUz;
    private String answerRu;
    private Boolean isAnswer = false;

    public String getAnswerUz() {
        return answerUz;
    }

    public void setAnswerUz(String answerUz) {
        this.answerUz = answerUz;
    }

    public String getAnswerRu() {
        return answerRu;
    }

    public void setAnswerRu(String answerRu) {
        this.answerRu = answerRu;
    }

    public Boolean getAnswer() {
        return isAnswer;
    }

    public void setAnswer(Boolean answer) {
        isAnswer = answer;
    }

    private CheckDto getParent(Locale locale) {
        CheckDto item = new CheckDto();
        item.setId(getId());
        item.setLabel(getAnswerUz());
        if (locale.getLanguage().equalsIgnoreCase("ru")) {
            item.setLabel(getAnswerRu());
        }

        item.setSelected(getAnswer());
        return item;
    }

    public CheckDto asCheckItem(Locale locale) {
        CheckDto item = getParent(locale);
        item.setSelected(false);

        item.setParent(getParent(locale));
        return item;
    }
}
