package uz.test.desktop.view;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.aspectj.weaver.ast.Test;
import uz.test.desktop.constants.DialogResponse;
import uz.test.desktop.dto.ResultDto;

public class Dialogs {

    private static DialogResponse buttonSelected = DialogResponse.CANCEL;

    private static ImageView icon = new ImageView();

    static class Dialog extends Stage {

        public Dialog(String title, Stage owner, Scene scene) {
            setTitle(title);
            initStyle(StageStyle.UTILITY);
            initModality(Modality.APPLICATION_MODAL);
            initOwner(owner);
            setResizable(false);
            setScene(scene);
        }

        public Dialog(String title, Stage owner, Scene scene, String iconFile) {
            setTitle(title);
            initStyle(StageStyle.UTILITY);
            initModality(Modality.APPLICATION_MODAL);
            initOwner(owner);
            setResizable(false);
            setScene(scene);
            icon.setImage(new Image(getClass().getResourceAsStream(iconFile)));
        }

        public void showDialog() {
            sizeToScene();
            centerOnScreen();
            showAndWait();
        }
    }

    static class Message extends Text {
        public Message(String msg) {
            super(msg);
            setWrappingWidth(300);
            setFont(Font.font(20D));
            setTextAlignment(TextAlignment.CENTER);
        }
    }

    public static DialogResponse showConfirmDialog(Stage owner, String message, String title, String yes, String no) {
        VBox vb = new VBox();
        vb.setPrefSize(400D, 150D);
        Scene scene = new Scene(vb);
        final Dialog dial = new Dialog(title, owner, scene, "/images/Help-icon.png");
        vb.setPadding(new Insets(10, 10, 10, 10));
        vb.setSpacing(10);
        Button yesButton = new Button(yes);
        yesButton.setPrefSize(100D, 45D);
        yesButton.setStyle("-fx-background-color: #4CAF50; -fx-font-size: 20px; -fx-font-weight: bold;");
        yesButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                dial.close();
                buttonSelected = DialogResponse.YES;
            }
        });
        Button noButton = new Button(no);
        noButton.setPrefSize(100D, 45D);
        noButton.setStyle("-fx-background-color: #A6A6A6; -fx-font-size: 20px; -fx-font-weight: bold;");
        noButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                dial.close();
                buttonSelected = DialogResponse.NO;
            }
        });
        BorderPane bp = new BorderPane();
        HBox buttons = new HBox();
        buttons.setAlignment(Pos.CENTER);
        buttons.setSpacing(10);
        buttons.getChildren().addAll(yesButton, noButton);
        bp.setCenter(buttons);
        HBox msg = new HBox();
        msg.setAlignment(Pos.CENTER);
        msg.setSpacing(5);
        msg.getChildren().addAll(icon, new Message(message));

        final Separator separator = new Separator();
        separator.setOrientation(Orientation.HORIZONTAL);

        vb.getChildren().addAll(msg, separator, bp);
        dial.showDialog();
        return buttonSelected;
    }

    public static void showMessageDialog(Stage owner, String message, String title) {
        showMessageDialog(owner, new Message(message), title);
    }

    public static void showMessageDialog(Stage owner, Node message, String title) {
        VBox vb = new VBox();
        Scene scene = new Scene(vb);
        final Dialog dial = new Dialog(title, owner, scene, "/images/info-icon.png");
        vb.setPadding(new Insets(10, 10, 10, 10));
        vb.setSpacing(10);
        Button okButton = new Button("OK");
        okButton.setAlignment(Pos.CENTER);
        okButton.setPrefSize(100D, 45D);
        okButton.setStyle("-fx-background-color: #A6A6A6; -fx-font-size: 20px; -fx-font-weight: bold;");
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                dial.close();
            }
        });
        BorderPane bp = new BorderPane();
        bp.setCenter(okButton);
        HBox msg = new HBox();
        msg.setAlignment(Pos.CENTER);
        msg.setSpacing(5);
        msg.getChildren().addAll(icon, message);

        final Separator separator = new Separator();
        separator.setOrientation(Orientation.HORIZONTAL);

        vb.getChildren().addAll(msg, separator, bp);
        dial.showDialog();
    }

    public static void showWarningDialog(Stage owner, String message, String title) {
        VBox vb = new VBox();
        Scene scene = new Scene(vb);
        final Dialog dial = new Dialog(title, owner, scene, "/images/warning-icon3.png");
        vb.setPadding(new Insets(10, 10, 10, 10));
        vb.setSpacing(10);
        JFXButton okButton = new JFXButton("OK");
        okButton.setButtonType(JFXButton.ButtonType.RAISED);
        Font font = Font.font("System", FontWeight.BOLD, 18);
        okButton.setFont(font);
        okButton.setAlignment(Pos.CENTER);
        okButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                dial.close();
            }
        });
        BorderPane bp = new BorderPane();
        bp.setCenter(okButton);
        HBox msg = new HBox();
        msg.setSpacing(5);

        Text text = new Text(message);
        text.setWrappingWidth(300);
        text.setFont(Font.font(22D));
        msg.getChildren().addAll(icon, text);

        final Separator separator = new Separator();
        separator.setOrientation(Orientation.HORIZONTAL);

        vb.getChildren().addAll(msg, separator, bp);
        dial.showDialog();
    }

    public static DialogResponse showResultDialog(Stage owner, String title, ResultDto result, String analysis, String repeat) {
        final BorderPane borderPane = new BorderPane();

        final GridPane grid = new GridPane();
        grid.setVgap(15);
        grid.setHgap(15);
        grid.setPadding(new Insets(15));

        Scene scene = new Scene(borderPane);
        final Dialog dialog = new Dialog(title, owner, scene);

        Text totalTxt = new Text(result.getTotalLabel());
        totalTxt.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(totalTxt, 1, 0);

        Text resultTxt = new Text(result.getAnswerLabel());
        resultTxt.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(resultTxt, 1, 1);

        Text mistakeTxt = new Text(result.getMistakeLabel());
        mistakeTxt.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(mistakeTxt, 1, 2);

        Text total = new Text(String.valueOf(result.getTotal()));
        total.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(total, 2, 0);

        Text resultCount = new Text(String.valueOf(result.getAnswer()));
        resultCount.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(resultCount, 2, 1);

        Text mistakeCount = new Text(String.valueOf(result.getMistake()));
        mistakeCount.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(mistakeCount, 2, 2);

        final PieChart chart = new PieChart();
        chart.setTitle(title);
        chart.setPadding(new Insets(15));

        PieChart.Data slice1 = new PieChart.Data(result.getTotalLabel(), result.getTotal());
        PieChart.Data slice2 = new PieChart.Data(result.getAnswerLabel(), result.getAnswer());
        PieChart.Data slice3 = new PieChart.Data(result.getMistakeLabel(), result.getMistake());

        chart.setPrefSize(600, 400);
        chart.getData().add(slice1);
        chart.getData().add(slice2);
        chart.getData().add(slice3);

        final VBox vb1 = new VBox();
        vb1.setPadding(new Insets(10, 10, 10, 10));
        vb1.setAlignment(Pos.CENTER);
        vb1.setSpacing(10);

        vb1.getChildren().addAll(grid, new Separator());

        borderPane.setTop(vb1);
        borderPane.setCenter(chart);

        final VBox vb = new VBox();
        vb.setPadding(new Insets(10, 10, 10, 10));
        vb.setAlignment(Pos.CENTER);
        vb.setSpacing(10);

        final HBox hb = new HBox();
        hb.setPadding(new Insets(10, 10, 10, 10));
        hb.setAlignment(Pos.CENTER);
        hb.setSpacing(10);

        JFXButton btnAnalysis = new JFXButton(analysis);
        btnAnalysis.setButtonType(JFXButton.ButtonType.RAISED);
        btnAnalysis.setStyle("-fx-background-color: #17A2B8;-fx-text-fill: white;-fx-font-size: 20px;-fx-cursor: hand;-fx-font-weight: bold;");

        btnAnalysis.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
                buttonSelected = DialogResponse.YES;
            }
        });

        JFXButton btnRepeat = new JFXButton(repeat);
        btnRepeat.setButtonType(JFXButton.ButtonType.RAISED);
        btnRepeat.setStyle("-fx-background-color:  #4CAF50;-fx-text-fill: white;-fx-font-size: 20px;-fx-cursor: hand;-fx-font-weight: bold;");

        btnRepeat.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
                buttonSelected = DialogResponse.NO;
            }
        });

        hb.getChildren().addAll(btnAnalysis, btnRepeat);

        vb.getChildren().addAll(new Separator(), hb);

        borderPane.setBottom(vb);

        dialog.setOnCloseRequest((e) -> {
            buttonSelected = DialogResponse.CLOSE;
        });

        dialog.showDialog();

        return buttonSelected;
    }
}
