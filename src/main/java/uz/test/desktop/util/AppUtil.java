package uz.test.desktop.util;

public class AppUtil {

    public static boolean isEmpty(String text) {
        return text == null || text.trim().isEmpty();
    }

    public static boolean nonEmpty(String text) {
        return text != null && !text.trim().isEmpty();
    }

    public static boolean isEmpty(Long id) {
        return id == null || id <= 0;
    }


    public static String searchKey(String searchTxt) {
        if (!AppUtil.isEmpty(searchTxt)) {
            return "%" + searchTxt.toLowerCase().trim()
                    .replaceAll("'", "''")
                    .replaceAll(" ", "%")
                    + "%";
        }
        return searchTxt;
    }
}
