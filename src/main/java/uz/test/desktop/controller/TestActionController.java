package uz.test.desktop.controller;

import com.jfoenix.controls.JFXButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import uz.test.desktop.config.StageManager;
import uz.test.desktop.constants.DialogResponse;
import uz.test.desktop.constants.ProgressColor;
import uz.test.desktop.domain.AnswersEntity;
import uz.test.desktop.dto.CheckDto;
import uz.test.desktop.dto.QuestionsDto;
import uz.test.desktop.dto.ResultDto;
import uz.test.desktop.repository.AnswersRepository;
import uz.test.desktop.service.QuestionsService;
import uz.test.desktop.util.ActionUtils;
import uz.test.desktop.util.Timer;
import uz.test.desktop.view.Dialogs;
import uz.test.desktop.view.FxmlView;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class TestActionController implements Initializable {

    @FXML
    private ToggleGroup toggleVariant;

    @FXML
    private TextArea textAreaQuestion;

    @FXML
    private TextArea textAreaA;

    @FXML
    private TextArea textAreaB;

    @FXML
    private TextArea textAreaV;

    @FXML
    private TextArea textAreaG;

    @FXML
    private JFXButton btnBack;

    @FXML
    private JFXButton btnNext;

    @FXML
    private JFXButton btnConfirm;

    @FXML
    private FontAwesomeIcon iconBtnNext;

    @FXML
    private FontAwesomeIcon iconBtnBack;

    @FXML
    private FontAwesomeIcon iconBtnConfirm;

    @FXML
    private Label lblQNumber;

    @FXML
    private Label lblTimer;

    @FXML
    private Label lblQTotal;

    @FXML
    private ProgressBar barTimer;

    @FXML
    private ProgressBar barQCount;

    @FXML
    private Label lblAllTimer;

    @FXML
    private BorderPane borderPaneV;

    @FXML
    private CheckBox checkboxHide;

    @FXML
    private BorderPane borderPaneHeader;

    @FXML
    private JFXButton btnFinish;

    @FXML
    private FontAwesomeIcon iconBtnFinish;

    @Autowired
    private AnswersRepository answersRepository;

    @Lazy
    @Autowired
    private StageManager stageManager;

    @Autowired
    private QuestionsService questionsService;

    private SettingsController settingsController;
    private List<QuestionsDto> questions;
    private List<CheckDto> results;
    private Timeline delayTimeline, threeTimeline;
    private static final String[] barColorStyleClasses = (String[]) ProgressColor.asColors().toArray(new String[ProgressColor.values().length]);
    private Timer currentTime = new Timer("00", "00", "00");
    private Timer finishTime;
    private int t = 0, k = 0, a = 0;
    private static final Integer MAX_TIME = 1;
    private Integer MAX_TIMES = 0;
    private int minutes = 0;
    private int seconds = 0;
    private int hours = 0;
    private int hoursAll = 0;
    private int minutesAll = 0;
    private int secondsAll = 0;
    private boolean btnNextDisable, btnBackDisable, btnConfirmDisable, btnFinishDisable = false;
    private Long answerId;
    private CheckDto currentQuestion;
    private List<QuestionsDto> hideQuestionsDeleteList;
    private List<CheckDto> resultMistakeQuestions;

    private ResourceBundle resourceBundle;

    public TestActionController(SettingsController settingsController) {
        this.settingsController = settingsController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle = resources;

        questions = new ArrayList<>();
        hideQuestionsDeleteList = new ArrayList<>();
        resultMistakeQuestions = new ArrayList<>();
        results = new ArrayList<>();
        barTimer = new ProgressBar();

        this.btnBackDisable = false;
        this.btnNextDisable = false;
        this.btnConfirmDisable = false;
        this.btnFinishDisable = false;

        Locale locale = resourceBundle.getLocale();

        btnBack.setCursor(Cursor.HAND);
        btnNext.setCursor(Cursor.HAND);
        btnConfirm.setCursor(Cursor.HAND);

        textAreaQuestion.setPrefRowCount(2);
        textAreaQuestion.setFont(Font.font(24));

        textAreaA.setPrefRowCount(2);
        textAreaA.setFont(Font.font(24));

        textAreaB.setPrefRowCount(2);
        textAreaB.setFont(Font.font(24));

        textAreaV.setPrefRowCount(2);
        textAreaV.setFont(Font.font(24));

        textAreaG.setPrefRowCount(2);
        textAreaG.setFont(Font.font(24));

        nodeDisabled(btnConfirm, iconBtnConfirm, "confirm");
        nodeDisabled(btnBack, iconBtnBack, "back");
        nodeDisabled(btnFinish, iconBtnFinish, "finish");

        checkboxHide.setStyle("-fx-font-size: 18px;");

        checkboxHide.selectedProperty().addListener(
                (ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) -> {
                    QuestionsDto dto = questions.get(t);
                    questionsService.updateByIdAndHiddenQuestions(dto.getId(), new_val);
                    dto.setHide(new_val);
                    if (new_val) {
                        hideQuestionsDeleteList.remove(dto);
                    } else {
                        hideQuestionsDeleteList.add(dto);
                    }
                });

        questions.addAll(settingsController.getQuestionsResultList());
        if (!questions.isEmpty()) {
            lblQTotal.setText("" + questions.size());
            results.addAll(questions.stream().map(dto -> dto.asCheckDto(locale)).collect(Collectors.toList()));
            startTime();
            playTest(t);
        }
    }

    private void startTime() {
        int time = questions.size() * MAX_TIME;
        MAX_TIMES = time;
        int minute = time % 60;
        hoursAll = time / 60;

        minutesAll = minute;
        minute = hoursAll * 60 + minutesAll;

        finishTime = new Timer();
        currentTime = new Timer();
        setAllTime();

        if (delayTimeline != null) {
            delayTimeline.stop();
        }

        delayTimeline = new Timeline();
        delayTimeline.getKeyFrames().addAll(
                new KeyFrame(Duration.seconds(0), e1 -> {

                    if (threeTimeline != null) {
                        threeTimeline.stop();
                    }
                    threeTimeline = new Timeline();
                    threeTimeline.setCycleCount(Timeline.INDEFINITE);
                    threeTimeline.getKeyFrames().addAll(new KeyFrame(Duration.seconds(1), e -> {
                        CountDownAllTime();
                    }));

                    threeTimeline.play();
                }, new KeyValue(barTimer.progressProperty(), 0)),
                new KeyFrame(Duration.minutes(minute), new KeyValue(barTimer.progressProperty(), 1))
        );
        delayTimeline.setCycleCount(Timeline.INDEFINITE);

        barTimer.progressProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                double progress = newValue == null ? 0 : newValue.doubleValue();
                if (progress < 0.4) {
                    setBarStyleClass(barTimer, ProgressColor.GREEN_BAR);
                } else if (progress < 0.6) {
                    setBarStyleClass(barTimer, ProgressColor.YELLOW_BAR);
                } else if (progress < 0.8) {
                    setBarStyleClass(barTimer, ProgressColor.ORANGE_BAR);
                } else {
                    setBarStyleClass(barTimer, ProgressColor.RED_BAR);
                }
            }

            private void setBarStyleClass(ProgressBar bar, ProgressColor barStyleClass) {
                bar.getStyleClass().removeAll(barColorStyleClasses);
                bar.getStyleClass().add(barStyleClass.getColor());
            }
        });

        delayTimeline.play();
    }

    private boolean isFinish() {
        int minute = hours * 60 + minutes;
        return MAX_TIMES.equals(minute);
    }

    private void stopTest() {
        if (!isFinish() && !ActionUtils.isCheckAllTest(results)) {
            Platform.runLater(() -> {
                DialogResponse response = Dialogs.showConfirmDialog(stageManager.getPrimaryStage(),
                        resourceBundle.getString("TestActionController.dialog.stopTest"),
                        resourceBundle.getString("Dialog.action.warning"),
                        resourceBundle.getString("Dialog.action.yes"),
                        resourceBundle.getString("Dialog.action.no"));

                if (response == DialogResponse.YES) {
                    stopTimer();
                    showResultDialog();
                }
            });

        } else {
            stopTimer();
            btnFinishActivated(btnFinish);

            if (isFinish()) {
                Platform.runLater(() -> {
                    Dialogs.showMessageDialog(
                            stageManager.getPrimaryStage(),
                            resourceBundle.getString("TestActionController.dialog.message.timeUp"),
                            resourceBundle.getString("Dialog.action.warning"));
                    showResultDialog();
                });
            } else {
                showResultDialog();
            }
        }
    }

    private void stopTimer() {
        this.delayTimeline.stop();
        this.threeTimeline.stop();
        this.t = 0;
        this.k = 0;
        this.a = 0;
    }

    private void playTest(int t) {
        currentQuestion = new CheckDto();
        currentQuestion = results.get(t);

        textAreaQuestion.setText(currentQuestion.getLabel() + " id: " + currentQuestion.getId());
        lblQNumber.setText("" + (t + 1));

        final GridPane gridPaneV = new GridPane();
        final ToggleGroup groupV = new ToggleGroup();

        gridPaneV.setAlignment(Pos.CENTER);
        gridPaneV.setPrefSize(65D, 353D);
        ColumnConstraints col0 = new ColumnConstraints();
        col0.setHalignment(HPos.CENTER);
        col0.setHgrow(Priority.SOMETIMES);
        col0.setMinWidth(10D);
        col0.setPrefWidth(65D);
        gridPaneV.getColumnConstraints().add(col0);

        RowConstraints row0 = new RowConstraints();
        row0.setMinHeight(10D);
        row0.setPrefHeight(30D);
        row0.setVgrow(Priority.SOMETIMES);

        gridPaneV.getRowConstraints().addAll(row0, row0, row0, row0);

        RadioButton rbA = new RadioButton("A");
        rbA.setToggleGroup(groupV);
        rbA.setStyle("-fx-font-size: 24px; -fx-cursor: hand; -fx-text-fill: white; -fx-font-weight: bold;");
        rbA.setUserData("A");
        rbA.setTextAlignment(TextAlignment.LEFT);

        RadioButton rbB = new RadioButton("Б");
        rbB.setToggleGroup(groupV);
        rbB.setStyle("-fx-font-size: 24px; -fx-cursor: hand; -fx-text-fill: white; -fx-font-weight: bold;");
        rbB.setUserData("B");

        RadioButton rbV = new RadioButton("В");
        rbV.setToggleGroup(groupV);
        rbV.setStyle("-fx-font-size: 24px; -fx-cursor: hand; -fx-text-fill: white; -fx-font-weight: bold;");
        rbV.setUserData("V");

        RadioButton rbG = new RadioButton("Г");
        rbG.setToggleGroup(groupV);
        rbG.setStyle("-fx-font-size: 24px; -fx-cursor: hand; -fx-text-fill: white; -fx-font-weight: bold;");
        rbG.setUserData("G");

        gridPaneV.add(rbA, 0, 0);
        gridPaneV.add(rbB, 0, 1);
        gridPaneV.add(rbV, 0, 2);
        gridPaneV.add(rbG, 0, 3);

        gridPaneV.setStyle("-fx-background-color: #808080;");

        Node node = this.borderPaneV.getLeft();
        if (node != null) {
            this.borderPaneV.setLeft(null);
        }
        this.borderPaneV.setLeft(gridPaneV);

        setVariants(currentQuestion, rbA, rbB, rbV, rbG);
        addHeaderFlowPane();

        groupV.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (groupV.getSelectedToggle() != null) {

                    btnConfirmActivated(btnConfirm);
                    String answer = groupV.getSelectedToggle().getUserData().toString();
                    List<CheckDto> variants = currentQuestion.getItems();
                    CheckDto answerDto = new CheckDto();

                    switch (answer) {
                        case "A":
                            answerDto = variants.get(0);
                            break;
                        case "B":
                            answerDto = variants.get(1);
                            break;
                        case "V":
                            answerDto = variants.get(2);
                            break;
                        case "G":
                            answerDto = variants.get(3);
                            break;

                    }

                    final Long id = answerDto.getId();
                    if (Objects.nonNull(answerId)) {
                        answerId = null;
                    }
                    answerId = id;
                }
            }
        });
    }

    private void addHeaderFlowPane() {
        final ObservableList<Node> observableList = FXCollections.observableArrayList();
        for (int j = 0; j < results.size(); j++) {
            CheckDto checkDto = results.get(j);
            Button btn = new Button();

            String txt = "" + (j + 1);
            btn.setText(txt);
            btn.getStyleClass().add("btn-default");
            btn.setPrefSize(46D, 46D);
            btn.setTooltip(new Tooltip(txt));

            boolean isSelect = checkDto.getItems().stream().anyMatch(CheckDto::isSelected);
            if (isSelect) {
                btn.getStyleClass().removeAll("btn-default", "btn-active", "btn-selected");
                btn.getStyleClass().add("btn-selected");
            }

            btn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    int action = Integer.valueOf(btn.getText());
                    a = action - 1;
                    if (a != t) {
                        playTest(a);
                    }
                    t = a;
                    btnNextActivated(btnNext);
                }
            });

            observableList.add(btn);
        }

        final FlowPane flow = new FlowPane();
        flow.setPadding(new Insets(5, 0, 5, 0));
        flow.setVgap(4);
        flow.setHgap(4);
        flow.setPrefWrapLength(200);
        flow.getChildren().addAll(observableList);
        flow.setAlignment(Pos.CENTER);

        final ScrollPane scroll = new ScrollPane();

        scroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scroll.setContent(flow);
        scroll.viewportBoundsProperty().addListener(new ChangeListener<Bounds>() {
            @Override
            public void changed(ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) {
                flow.setPrefWidth(newValue.getWidth());
                flow.setPrefHeight(newValue.getHeight());
            }
        });

        Node node = this.borderPaneHeader.getCenter();
        if (node != null) {
            this.borderPaneHeader.setCenter(null);
        }
        this.borderPaneHeader.setCenter(scroll);
    }

    private void CountDownAllTime() {
        if (secondsAll == 0) {
            minutesAll--;
            secondsAll = 59;
        } else {
            secondsAll--;
        }

        if (minutesAll == 0 && hoursAll > 0) {
            hoursAll--;
            minutesAll = 59;
        }

        if (seconds == 59) {
            minutes++;
            seconds = 0;
        } else {
            seconds++;
        }

        if (minutes == 60) {
            minutes = 0;
            hours++;
        }

        setAllTime();
    }

    private void setAllTime() {

        if (isFinish()) {
            stopTest();
        }

        currentTime.setSeconds(seconds);
        currentTime.setMinutes(minutes);
        currentTime.setHours(hours);
//        lblTimer.setText(currentTime.toString());

        finishTime.setSeconds(secondsAll);
        finishTime.setMinutes(minutesAll);
        finishTime.setHours(hoursAll);
        lblAllTimer.setText(finishTime.toString());
    }

   /* private void questionProgress(int t) {
        qProgress = (double) (t + 1) / questions.size();
        barQCount.setProgress(qProgress);
    } */

    private void setVariants(CheckDto currentQuestion, RadioButton rbA, RadioButton rbB, RadioButton rbV, RadioButton rbG) {
        int i = 0;
        for (CheckDto variant : currentQuestion.getItems()) {
            String text = variant.getLabel();
            switch (i) {
                case 0:
                    textAreaA.setText(text);
                    rbA.setDisable(false);
                    rbA.setSelected(variant.isSelected());
                    break;
                case 1:
                    textAreaB.setText(text);
                    rbB.setDisable(false);
                    rbB.setSelected(variant.isSelected());
                    break;
                case 2:
                    textAreaV.setText(text);
                    rbV.setDisable(false);
                    rbV.setSelected(variant.isSelected());
                    break;
                case 3:
                    textAreaG.setText(text);
                    rbG.setDisable(false);
                    rbG.setSelected(variant.isSelected());
                    break;
            }
            i++;
        }
    }

    @FXML
    void onBack(ActionEvent event) {
        if (!this.btnBackDisable) {
            if (k > 0 && t > 0) {
                btnNextActivated(btnNext);
                k = t;
                t--;
                playTest(t);
            }
            if (t == 0) {
                nodeDisabled(btnBack, iconBtnBack, "back");
            }
        }
    }

    @FXML
    void onBtnConfirm(ActionEvent event) {
        if (!this.btnConfirmDisable) {
            List<CheckDto> variants = currentQuestion.getItems();
            List<CheckDto> answers = variants.stream().map(ans -> {
                if (ans.getId().equals(answerId)) {
                    ans.setSelected(true);
                    return ans;
                }
                ans.setSelected(false);
                return ans;
            }).collect(Collectors.toList());

            currentQuestion.getItems().clear();
            currentQuestion.getItems().addAll(answers);
            results.remove(t);
            results.add(t, currentQuestion);

            if (ActionUtils.isCheckTest(results)) btnFinishActivated(btnFinish);

            if (ActionUtils.isCheckAllTest(results)) {
                stopTest();
                return;
            }
            addHeaderFlowPane();
        }
    }

    @FXML
    void onBtnNext(ActionEvent event) {
        if (!this.btnNextDisable) {
            next();
        }
    }

    @FXML
    void onBtnFinish(ActionEvent event) {
        stopTest();
    }

    private void clearTimer() {
        this.seconds = 0;
        this.minutes = 0;
        this.hours = 0;
        this.secondsAll = 0;
        this.minutesAll = 0;
        this.hoursAll = 0;
    }

    private void showResultDialog() {
        if (!this.btnFinishDisable) {

            List<Long> answerIds = ActionUtils.getAnswerIds(results);

            Long count = answersRepository.getAnswerCount(answerIds);
            Long total = (long) results.size();
            Long resultTotal = 0L;
            Long mistake = 0L;
            if (count >= 0) {
                resultTotal = count;
                mistake = total - count;
            }

            DialogResponse response = Dialogs.showResultDialog(
                    stageManager.getPrimaryStage(),
                    resourceBundle.getString("TestActionController.dialog.result.title"),
                    new ResultDto(
                            total, resultTotal, mistake,
                            resourceBundle.getString("TestActionController.dialog.result.total"),
                            resourceBundle.getString("TestActionController.dialog.result.answer"),
                            resourceBundle.getString("TestActionController.dialog.result.mistake")),
                    resourceBundle.getString("TestActionController.dialog.result.btnAnalysis"),
                    resourceBundle.getString("TestActionController.dialog.result.btnRepeat"));

            if (response == DialogResponse.CLOSE) {
                clearTimer();
                loadWindow(FxmlView.SETTINGS, resourceBundle);
            }

            if (response == DialogResponse.YES) {

                List<AnswersEntity> correctAnswers = answersRepository.getCorrectAnswerList(answerIds);

                List<Long> correctQuestionIds = correctAnswers.stream().map(AnswersEntity::getQuestionId).collect(Collectors.toList());

                List<CheckDto> mistakeQuestions = ActionUtils.getMistakeQuestions(correctQuestionIds, results);
                resultMistakeQuestions.addAll(mistakeQuestions);

                loadWindow(FxmlView.COMMENT, resourceBundle);
            }
        }
    }

    private void next() {
        nodeDisabled(btnConfirm, iconBtnConfirm, "confirm");
        k = t;
        k++;
        if (k < questions.size()) {
            btnBackActivated(btnBack);
            t = k;
            playTest(t);
        }

        if (k > questions.size() || k == questions.size() - 1) {
            nodeDisabled(btnNext, iconBtnNext, "next");
        }
    }

    private void nodeDisabled(Node node, FontAwesomeIcon icon, String nodeName) {
        node.setStyle("-fx-background-color: #D1D1D1; -fx-background-radius: 25px; -fx-text-fill: #9B9B9B;");
        node.setCursor(Cursor.DEFAULT);
        icon.setFill(Paint.valueOf("#9B9B9B"));

        if ("next".equals(nodeName)) {
            this.btnNextDisable = true;
        } else if ("confirm".equals(nodeName)) {
            this.btnConfirmDisable = true;
        } else if ("back".equals(nodeName)) {
            this.btnBackDisable = true;
        } else if ("finish".equals(nodeName)) {
            this.btnFinishDisable = true;
        }
    }

    private void btnBackActivated(Node node) {
        node.setStyle("-fx-background-color:  #FF7B30; -fx-background-radius: 25px; -fx-text-fill: #ffffff;");
        node.setCursor(Cursor.HAND);
        iconBtnBack.setFill(Paint.valueOf("#ffffff"));
//        for (QuestionsDto dto : questions) {
            checkboxHide.setSelected(questions.get(t).isHide());
//        }
        this.btnBackDisable = false;
    }

    private void btnNextActivated(Node node) {
        node.setStyle("-fx-background-color:  #008e9b; -fx-background-radius: 25px; -fx-text-fill: #ffffff;");
        node.setCursor(Cursor.HAND);
        iconBtnNext.setFill(Paint.valueOf("#ffffff"));
        System.out.println(questions.get(t).getQuestionUz());
//        for (QuestionsDto dto : questions) {
            checkboxHide.setSelected(questions.get(t).isHide());
//        }
        this.btnNextDisable = false;
    }

    private void btnConfirmActivated(Node node) {
        node.setStyle("-fx-background-color:   #4CAF50; -fx-background-radius: 25px; -fx-text-fill: #ffffff;");
        node.setCursor(Cursor.HAND);
        iconBtnConfirm.setFill(Paint.valueOf("#ffffff"));
        this.btnConfirmDisable = false;
    }

    private void btnFinishActivated(Node node) {
        node.setStyle("-fx-background-color:   #DC3545; -fx-background-radius: 25px; -fx-text-fill: #ffffff;");
        node.setCursor(Cursor.HAND);
        iconBtnFinish.setFill(Paint.valueOf("#ffffff"));
        this.btnFinishDisable = false;
    }

    private void loadWindow(FxmlView fxmlView, ResourceBundle bundle) {
        stageManager.switchScene(fxmlView,bundle);
    }

    public List<CheckDto> getResultMistakeQuestions() {
        return resultMistakeQuestions;
    }
}
