package uz.test.desktop.util;

public class Timer {

    private String hours;
    private String minutes;
    private String seconds;

    public Timer() {
    }

    public Timer(String hours, String minutes, String seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public String getMinutes() {
        return this.minutes;
    }

    public String getSeconds() {
        return this.seconds;
    }

    public void setMinutes(int min) {
        if (min < 10) {
            this.minutes = "0" + min;
        } else {
            this.minutes = min + "";
        }
    }

    public void setSeconds(int sec) {
        if (sec < 10) {
            this.seconds = "0" + sec;
        } else {
            this.seconds = sec + "";
        }
    }

    public String getHours() {
        return hours;
    }

    public void setHours(int hour) {
        if (hour < 10)     {
            this.hours = "0" + hour;
        } else {
            this.hours = hour + "";
        }
    }

    public String toString() {
        return this.hours + ":" + this.minutes + ":" + this.seconds;
    }
}
