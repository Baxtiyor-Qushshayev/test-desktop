package uz.test.desktop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.test.desktop.domain.AnswersEntity;
import java.util.List;

@Repository
public interface AnswersRepository extends JpaRepository<AnswersEntity, Long> {

    @Query(value = "SELECT * FROM ANSWERS A " +
            "where A.QUESTION_ID=:questionId " +
            "ORDER BY RAND() DESC", nativeQuery = true)
    List<AnswersEntity> getByAnswersIdList(@Param("questionId") Long questionId);

    @Query(value = "SELECT COUNT(DISTINCT a.id) FROM AnswersEntity a WHERE (a.id IN :ids) and a.isAnswer = true")
    Long getAnswerCount(@Param("ids") List<Long> ids);

    @Query(value = "SELECT a FROM AnswersEntity a WHERE (a.id IN :ids) and a.isAnswer = true")
    List<AnswersEntity> getCorrectAnswerList(@Param("ids") List<Long> ids);
}
