package uz.test.desktop.util;

import uz.test.desktop.dto.CheckDto;

import java.util.ArrayList;
import java.util.List;

public class ActionUtils {

    public static List<Long> getAnswerIds(List<CheckDto> results) {
        List<Long> answerIds = new ArrayList<>();
        for (CheckDto checkDto : results) {
            for (CheckDto resultDto : checkDto.getItems()) {
                if (resultDto.isSelected()) {
                    answerIds.add(resultDto.getId());
                }
            }
        }
        return answerIds;
    }

    public static boolean isCheckAllTest(List<CheckDto> results) {
        List<Long> answerIds = new ArrayList<>();
        for (CheckDto checkDto : results) {
            for (CheckDto resultDto : checkDto.getItems()) {
                if (resultDto.isSelected()) {
                    answerIds.add(resultDto.getId());
                }
            }
        }

        Long size = (long) results.size();
        Long sizeAns = (long) answerIds.size();

        return size.equals(sizeAns);
    }

    public static boolean isCheckTest(List<CheckDto> results) {
        List<Long> answerIds = new ArrayList<>();
        for (CheckDto checkDto : results) {
            for (CheckDto resultDto : checkDto.getItems()) {
                if (resultDto.isSelected()) {
                    answerIds.add(resultDto.getId());
                }
            }
        }
        return answerIds.size() > 0;
    }

    public static List<CheckDto> getMistakeQuestions(List<Long> ids, List<CheckDto> results) {
        for (Long id: ids) {
            CheckDto dto = getCheckDto(id, results);
            if (dto != null) {
                results.remove(dto);
            }
        }
        return results;
    }

    private static CheckDto getCheckDto(Long id, List<CheckDto> results) {
        for (CheckDto checkDto : results) {
            if (checkDto.getId().equals(id)) {
                return checkDto;
            }
        }
        return null;
    }
}
