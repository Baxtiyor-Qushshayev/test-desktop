package uz.test.desktop.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.test.desktop.domain.AnswersEntity;
import uz.test.desktop.repository.AnswersRepository;
import uz.test.desktop.repository.QuestionsRepository;
import uz.test.desktop.service.AnswersService;

import java.util.List;


@Service
public class AnswersServiceImpl implements AnswersService {

    private static final Logger LOG = LoggerFactory.getLogger(AnswersServiceImpl.class);

    @Autowired
    private QuestionsRepository questionsRepository;

    @Autowired
    private AnswersRepository answersRepository;


    @Override
    public List<AnswersEntity> findByQuestionId(Long questionId) {
        List<AnswersEntity> answersEntityList = answersRepository.getByAnswersIdList(questionId);
        return answersEntityList;
    }
}
