package uz.test.desktop.util;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import uz.test.desktop.dto.AnswersDto;
import uz.test.desktop.dto.QuestionsDto;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;


public class ExcelReader {

    public static final String XLSX_FILE_PATH = "C:\\Users\\Dell\\Desktop\\Shablon.xlsx";

    public static List<QuestionsDto> getExcel() throws IOException, InvalidFormatException {
        InputStream file = new FileInputStream(XLSX_FILE_PATH);
        Workbook workbook = WorkbookFactory.create(file);
        Sheet sheet = workbook.getSheetAt(0);

        List<QuestionsDto> questionsList = new ArrayList<QuestionsDto>();
        int i = 0;
        for (Row row : sheet) {
            i++;
            if (i == 1) continue;
            int col = 0;
            List<AnswersDto> answersList = new ArrayList<AnswersDto>();
            QuestionsDto questionsDto = new QuestionsDto();
            AnswersDto answersDto1 = new AnswersDto();
            AnswersDto answersDto2 = new AnswersDto();
            AnswersDto answersDto3 = new AnswersDto();
            AnswersDto answersDto4 = new AnswersDto();
            for (Cell cell : row) {
                col++;
                switch (col) {
                    //---Тест рақамлари start--------
                    case 1:
                        questionsDto.setTestNumbers(getString(cell).toString());
                        break;
                    //---Тест рақамлари stop

                    //---Тест саволллари (uz) start-----
                    case 2: {
                        questionsDto.setQuestionUz(getString(cell).toString());
                        break;
                    }
                    //---Тест саволллари (uz) stop

                    //---Тест саволллари (ru) start------
                    case 3: {
                        questionsDto.setQuestionRu(getString(cell).toString());
                        break;
                    }
                    //---Тест саволллари (ru) stop

                    //---Тўғри жавоблар start-----
                    case 4: {
                        answersDto1.setAnswerUz(getString(cell).toString());
                        break;
                    }
                    case 5: {
                        answersDto1.setAnswerRu(getString(cell).toString());
                        answersDto1.setAnswer(true);
                        answersList.add(answersDto1);
                        break;
                    }
                    //---Тўғри жавоблар stop

                    //---Нотўғри жавоблар1 start------
                    case 6: {
                        answersDto2.setAnswerUz(getString(cell).toString());
                        break;
                    }
                    case 7: {
                        answersDto2.setAnswerRu(getString(cell).toString());
                        answersList.add(answersDto2);
                        break;
                    }
                    //---Нотўғри жавоблар1 stop

                    //---Нотўғри жавоблар2 start------
                    case 8: {
                        answersDto3.setAnswerUz(getString(cell).toString());
                        break;
                    }
                    case 9: {
                        answersDto3.setAnswerRu(getString(cell).toString());
                        answersList.add(answersDto3);
                        break;
                    }
                    //---Нотўғри жавоблар2 stop

                    //---Нотўғри жавоблар3 start------
                    case 10: {
                        answersDto4.setAnswerUz(getString(cell).toString());
                        break;
                    }
                    case 11: {
                        answersDto4.setAnswerRu(getString(cell).toString());
                        answersList.add(answersDto4);
                        questionsDto.setAnswers(answersList);
                        break;
                    }
                    //---Нотўғри жавоблар3 stop

                    //---Изоҳ (uz) start-----
                    case 12: {
                        questionsDto.setCommentUz(getString(cell).toString());
                        break;
                    }
                    //---Изоҳ (uz) stop

                    //---Изоҳ(ru) start-----
                    case 13: {
                        questionsDto.setCommentRu(getString(cell).toString());
                        break;
                    }
                    //---Изоҳ (ru) stop

                    //---Қисмлар (uz) start-----
                    case 14: {
                        questionsDto.setPartUz(getString(cell).toString());
                        break;
                    }
                    //---Қисмлар (uz) stop

                    //---Қисмлар (ru) start-----
                    case 15: {
                        questionsDto.setPartRu(getString(cell).toString());
                        break;
                    }
                    //---Қисмлар (ru) stop

                    //---Боблар (uz) start-----
                    case 16: {
                        questionsDto.setChapterUz(getString(cell).toString());
                        break;
                    }
                    //---Боблар (uz) stop

                    //---Боблар (ru) start-----
                    case 17: {
                        questionsDto.setChapterRu(getString(cell).toString());
                        break;
                    }
                    //---Боблар (ru) stop

                    //---Бўлим (uz) start-----
                    case 18: {
                        questionsDto.setDepartment(getString(cell).toString());
                        break;
                    }
                    //---Бўлим (uz) stop

                    //---Бўлим (ru) start-----
                    case 19: {
                        questionsDto.setDepartmentId(new Double(getString(cell).toString()).longValue());
                        break;
                    }
                    //---Бўлим (ru) stop

                }
            }

            questionsList.add(questionsDto);
        }

        workbook.close();
        return questionsList;
    }

    private static Object getString(Cell cell) {
        switch (cell.getCellTypeEnum()) {
            case STRING:
                return cell.getRichStringCellValue() != null ? cell.getRichStringCellValue().getString() : null;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue();
                } else {
                    return cell.getNumericCellValue();
                }
            default:
                return null;
        }
    }

}
