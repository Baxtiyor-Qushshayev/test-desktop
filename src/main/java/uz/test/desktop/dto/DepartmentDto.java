package uz.test.desktop.dto;

import uz.test.desktop.dto.base.BaseDto;

import java.util.ArrayList;
import java.util.List;

public class DepartmentDto extends BaseDto {

    private String nameUz;
    private String nameRu;
    private String code;

    private List<QuestionsDto> questionsList;

    public String getNameUz() {
        return nameUz;
    }

    public void setNameUz(String nameUz) {
        this.nameUz = nameUz;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<QuestionsDto> getQuestionsList() {
        if (getQuestionsList() == null) {
            questionsList = new ArrayList<>();
        }
        return questionsList;
    }

    public void setQuestionsList(List<QuestionsDto> questionsList) {
        this.questionsList = questionsList;
    }
}
