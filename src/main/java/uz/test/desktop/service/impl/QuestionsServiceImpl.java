package uz.test.desktop.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.test.desktop.domain.AnswersEntity;
import uz.test.desktop.domain.QuestionsEntity;
import uz.test.desktop.dto.AnswersDto;
import uz.test.desktop.dto.HideQuestions;
import uz.test.desktop.dto.QuestionsDto;
import uz.test.desktop.dto.common.FilterData;
import uz.test.desktop.dto.common.SimpleTreeItem;
import uz.test.desktop.repository.AnswersRepository;
import uz.test.desktop.repository.QuestionsRepository;
import uz.test.desktop.service.QuestionsService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class QuestionsServiceImpl implements QuestionsService {

    private static final Logger LOG = LoggerFactory.getLogger(QuestionsServiceImpl.class);

    private final static int READ_START_POS = 0;

    private int startReadPos = READ_START_POS;

    public QuestionsServiceImpl() {
    }

    public QuestionsServiceImpl(int startReadPos) {
        if (startReadPos > 0) {
            this.startReadPos = startReadPos;
        }
    }


    @Autowired
    private QuestionsRepository questionsRepository;

    @Autowired
    private AnswersRepository answersRepository;

    @PersistenceContext
    protected EntityManager em;

    @Override
    @Transactional
    public void save(List<QuestionsDto> questionsDtos) {

        for (QuestionsDto questionsDto : questionsDtos) {
            QuestionsEntity questionsEntity = new QuestionsEntity();
            questionsEntity.setCode(questionsDto.getTestNumbers());
            questionsEntity.setQuestionUz(questionsDto.getQuestionUz());
            questionsEntity.setQuestionRu(questionsDto.getQuestionRu());
            questionsEntity.setCommentUz(questionsDto.getCommentUz());
            questionsEntity.setCommentRu(questionsDto.getCommentRu());
            questionsEntity.setChapterUz(questionsDto.getChapterUz());
            questionsEntity.setChapterRu(questionsDto.getChapterRu());
            questionsEntity.setPartUz(questionsDto.getPartUz());
            questionsEntity.setPartRu(questionsDto.getPartRu());
            questionsEntity.setPartRu(questionsDto.getPartRu());
            questionsEntity.setDepartmentId(questionsDto.getDepartmentId());
            QuestionsEntity questionsNew = questionsRepository.save(questionsEntity);

            for (AnswersDto answersDto : questionsDto.getAnswers()) {
                AnswersEntity answersEntity = new AnswersEntity();
                answersEntity.setAnswerUz(answersDto.getAnswerUz());
                answersEntity.setAnswerRu(answersDto.getAnswerRu());
                answersEntity.setAnswer(answersDto.getAnswer());
                answersEntity.setQuestionId(questionsNew.getId());
                answersRepository.save(answersEntity);
            }

        }
    }

    @Override
    public List<QuestionsEntity> findByDepartmentId(Long departmentId) {
        List<QuestionsEntity> questionsList = questionsRepository.getByDepartmentIdList(departmentId, 5);
        return questionsList;
    }

    @Override
    @Transactional(readOnly = true)
    public List<SimpleTreeItem> getTreeItemList(FilterData filter,boolean isLangChange) {
        List<QuestionsEntity> entQuestionsList = questionsRepository.findAll();
        if (filter.isSearchValid()) {
            entQuestionsList = questionsRepository.findByNameFilter(filter.getSearch());
            if (isLangChange) {
                entQuestionsList = questionsRepository.findByNameFilterRu(filter.getSearch());
            }

        }
        return entQuestionsList.stream().map(QuestionsEntity::asTreeItemDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<QuestionsDto> findByDepartmentCode(String depCode, int limit) {
        List<QuestionsEntity> questions = questionsRepository.findByDepartmentCode(depCode);

        return questions.stream().map(QuestionsEntity::asDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<QuestionsDto> findBySelectedQuestion(List<Long> ids) {
        List<QuestionsEntity> questions = questionsRepository.findBySelectedQuestionId(ids);

        return questions.stream().map(QuestionsEntity::asDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<QuestionsDto> findByHiddenQuestions() {
        List<QuestionsEntity> entQuestionsList = questionsRepository.findByHiddenQuestions();
        return entQuestionsList.stream().map(QuestionsEntity::asQuestionsDto).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void updateByHiddenQuestions(List<HideQuestions> list) {
        for (HideQuestions dto : list) {
            QuestionsEntity questionsEntity = questionsRepository.getOne(Long.valueOf(dto.getId()));
            questionsEntity.setHide(dto.isIshidel());
            questionsRepository.save(questionsEntity);
        }
    }

    @Override
    @Transactional
    public void updateByIdAndHiddenQuestions(Long id, Boolean aBoolean) {
            QuestionsEntity questionsEntity = questionsRepository.getOne(id);
            questionsEntity.setHide(aBoolean.booleanValue());
            questionsRepository.save(questionsEntity);
    }
}
