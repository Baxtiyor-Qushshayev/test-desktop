package uz.test.desktop.constants;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum DepartmentType {
    DEP_CIVIL("Ўзбекистон Республикасининг Фуқаролик кодекси"),
    DEP_WORK("Ўзбекистон Республикасининг меҳнат кодекси"),
    DEP_STOCK_MARKET("Қимматли қоғозлар бозори тўғрисида” Ўзбекистон Республикаси Қарори"),
    DEP_LIMITED("Масъулияти чекланган ҳамда қўшимча масъулиятли  жамиятлар тўғрисида” Ўзбекистон Республикаси Қарори");

    String label;

    DepartmentType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public String getName() {
        return name();
    }

    public static List<String> asList() {
        return Arrays.stream(values()).map(DepartmentType::getName).collect(Collectors.toList());
    }
}
