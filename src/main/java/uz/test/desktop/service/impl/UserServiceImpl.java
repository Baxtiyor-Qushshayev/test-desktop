package uz.test.desktop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.test.desktop.domain.UserEntity;
import uz.test.desktop.repository.UserRepository;
import org.springframework.transaction.annotation.Transactional;
import uz.test.desktop.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override

    @Transactional
    public void save() {

        UserEntity userEntity = new UserEntity();
        userEntity.setFirstName("Farxod");
        userEntity.setLastName("Babajanov");

        userRepository.save(userEntity);
    }
}
