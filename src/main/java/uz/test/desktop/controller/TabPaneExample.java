package uz.test.desktop.controller;

import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import org.springframework.beans.factory.annotation.Autowired;
import uz.test.desktop.dto.HideQuestions;
import uz.test.desktop.dto.QuestionsDto;
import uz.test.desktop.service.QuestionsService;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Lawrence PremKumar
 */
public class TabPaneExample extends Stage {

    private static boolean isLangChange;

    static class TabPaneDialog extends Stage {

        public TabPaneDialog(Stage owner, Scene scene) {
            initStyle(StageStyle.UTILITY);
            initOwner(owner);
            setResizable(false);
            setScene(scene);
        }

        public void showTabPaneDialog() {
            sizeToScene();
            centerOnScreen();
            showAndWait();
        }
    }


    public static void showDialog(Stage owner, String title, QuestionsService questionsService, ResourceBundle resourceBundle) {
        // Create a TabPane

        isLangChange = false;
        Locale locale = resourceBundle.getLocale();
        if (locale.getLanguage().equalsIgnoreCase("ru")) {
            isLangChange = true;
        }

        TabPane tabPane = new TabPane();
        tabPane.setStyle("-fx-background-color: red;");
        tabPane.setMinSize(Region.USE_PREF_SIZE, Region.USE_PREF_SIZE);
        CheckBox headerCheckBox = new CheckBox();

        ObservableList<HideQuestions> data;
        final TableView<HideQuestions> table = new TableView<HideQuestions>();

        List<HideQuestions> hideQuestionsDeleteList = new ArrayList<>();
        List<HideQuestions> players = new ArrayList<>();
        List<QuestionsDto> results = questionsService.findByHiddenQuestions();

        headerCheckBox.setSelected(results.size() > 0);

        for (QuestionsDto questionsDto : results) {
            players.add(new HideQuestions(questionsDto.isHide(), String.valueOf(questionsDto.getId()), isLangChange ? questionsDto.getQuestionRu() : questionsDto.getQuestionUz()));
        }

        data = FXCollections.<HideQuestions>observableArrayList(new Callback<HideQuestions, Observable[]>() {
                                                                    @Override
                                                                    public Observable[] call(HideQuestions player) {
                                                                        return new Observable[]{player.ishideProperty()};
                                                                    }
                                                                }
        );
        data.addAll(players);

        table.setItems(data);

        boolean finalIsLangChange1 = isLangChange;
        ChangeListener<Boolean> headerListener = (obs, ov, nv) -> {
            Platform.runLater(() -> {
                List<HideQuestions> playersNew = new ArrayList<>();
                List<QuestionsDto> resultsNew = questionsService.findByHiddenQuestions();
                for (QuestionsDto questionsDto : resultsNew) {
                    playersNew.add(new HideQuestions(nv, String.valueOf(questionsDto.getId()), finalIsLangChange1 ? questionsDto.getQuestionRu() : questionsDto.getQuestionUz()));
                }
                if (nv) {
                    hideQuestionsDeleteList.removeAll(hideQuestionsDeleteList);
                } else {
                    hideQuestionsDeleteList.addAll(playersNew);
                }
                table.getItems().clear();
                table.getItems().addAll(playersNew);
            });
        };

        TableColumn<HideQuestions, Boolean> ishideColumn = new TableColumn<>();
        headerCheckBox.selectedProperty().addListener(headerListener);
        ishideColumn.setGraphic(headerCheckBox);
        TableColumn<HideQuestions, String> idColumn = new TableColumn<>(resourceBundle.getString("TestActionController.footer.currentTxt"));
        TableColumn<HideQuestions, String> nameColumn = new TableColumn<>(resourceBundle.getString("TestActionController.question.name"));
        nameColumn.setPrefWidth(885);
        ishideColumn.setCellValueFactory(new PropertyValueFactory<HideQuestions, Boolean>("ishide"));
        Callback<TableColumn<HideQuestions, Boolean>, TableCell<HideQuestions, Boolean>> cellFactory = CheckBoxTableCell.forTableColumn(ishideColumn);
        ishideColumn.setCellFactory(new Callback<TableColumn<HideQuestions, Boolean>, TableCell<HideQuestions, Boolean>>() {
            @Override
            public TableCell<HideQuestions, Boolean> call(TableColumn<HideQuestions, Boolean> column) {
                TableCell<HideQuestions, Boolean> cell = cellFactory.call(column);
                cell.setAlignment(Pos.CENTER_LEFT);
                return cell;
            }
        });

        data.addListener((ListChangeListener.Change<? extends HideQuestions> c) -> {
            while (c.next()) {
                if (c.wasUpdated()) {
                    for (int i = c.getFrom(); i < c.getTo(); i++) {
                        HideQuestions hideQuestions = (HideQuestions) c.getList().get(i);
                        if (hideQuestions.isIshidel()) {
                            for (HideQuestions loan : hideQuestionsDeleteList) {
                                if (loan.getId().equals(hideQuestions.getId())) {
                                    hideQuestionsDeleteList.remove(hideQuestions);
                                    break;
                                }
                            }
                        } else {
                            hideQuestionsDeleteList.add(hideQuestions);
                        }
                    }
                }
            }
        });

        ishideColumn.setCellFactory(cellFactory);
        ishideColumn.setEditable(true);

        idColumn.setCellValueFactory(new PropertyValueFactory<HideQuestions, String>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<HideQuestions, String>("name"));

        idColumn.setCellFactory(tc -> {
            TableCell<HideQuestions, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);
            cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
            text.textProperty().bind(cell.itemProperty());
            text.setFill(Color.BLACK);
            text.setFont(Font.font("Verdana", 15));
            return cell;
        });
        nameColumn.setCellFactory(tc -> {
            TableCell<HideQuestions, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);
            cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
            text.wrappingWidthProperty().bind(nameColumn.widthProperty());
            text.textProperty().bind(cell.itemProperty());
            text.setFill(Color.BLACK);
            text.setFont(Font.font("Verdana", 16));
            return cell;
        });

        table.setEditable(true);
        table.getColumns().addAll(ishideColumn, idColumn, nameColumn);

        Tab tab = new Tab(resourceBundle.getString("TestActionController.dialog.unallowedQuestion"));
        tab.setClosable(false);
        tab.setContent(table);
        tabPane.getTabs().add(tab);

        Button button = new Button();

        button.setText(resourceBundle.getString("TestActionController.dialog.unblocked"));

        Scale scaleTransformation = new Scale();
        scaleTransformation.setPivotX(0);
        scaleTransformation.setPivotY(0);

        button.getTransforms().add(scaleTransformation);
        button.setMaxWidth(Double.MAX_VALUE);
        button.setPadding(new Insets(4, 2, 2, 2));
        VBox vBox = new VBox(tabPane);
        vBox.setSpacing(10);
        vBox.setPadding(new Insets(0, 2, 5, 2));
        vBox.getChildren().addAll(button);

        Scene scene = new Scene(vBox, 1000, 480);

        TabPaneDialog dialog = new TabPaneDialog(owner, scene);



        button.setOnAction((event) -> {
            if (hideQuestionsDeleteList.size() > 0) {
                questionsService.updateByHiddenQuestions(hideQuestionsDeleteList);
                hideQuestionsDeleteList.removeAll(hideQuestionsDeleteList);
                List<HideQuestions> playersNew = new ArrayList<>();
                List<QuestionsDto> resultsNew = questionsService.findByHiddenQuestions();
                for (QuestionsDto questionsDto : resultsNew) {
                        playersNew.add(new HideQuestions(questionsDto.isHide(), String.valueOf(questionsDto.getId()),  isLangChange ? questionsDto.getQuestionRu() : questionsDto.getQuestionUz()));
                }
                table.getItems().clear();
                table.getItems().addAll(playersNew);
            }
        });


        dialog.showTabPaneDialog();
    }

}