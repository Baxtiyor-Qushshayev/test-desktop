package uz.test.desktop.controller;

import net.rgielen.fxweaver.core.FxmlView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@FxmlView("main-stage.fxml")
public class MyController {
    private static final Logger log = LoggerFactory.getLogger(MyController.class);

//    @Autowired
//    private UserService userService;
//
//    @Autowired
//    private UserRepository userRepository;
//
//
//    @FXML
//    private Label weatherLabel;
//
//    public void loadWeatherForecast(ActionEvent actionEvent) {
//         try {
//            ExcelReader.getExcel();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (InvalidFormatException e) {
//            e.printStackTrace();
//        }
//        userService.save();
//        List<UserEntity> userEntityList =  userRepository.findAll();
//
//        log.info("Users: {}", userEntityList);
//
//
//        this.weatherLabel.setText("HELLO");
//    }
}
