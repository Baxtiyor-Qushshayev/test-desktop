package uz.test.desktop.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import uz.test.desktop.config.StageManager;
import uz.test.desktop.constants.Category;
import uz.test.desktop.constants.DepartmentType;
import uz.test.desktop.dto.*;
import uz.test.desktop.dto.common.FilterData;
import uz.test.desktop.dto.common.RandomData;
import uz.test.desktop.dto.common.SimpleTreeItem;
import uz.test.desktop.service.DepartmentService;
import uz.test.desktop.service.QuestionsService;
import uz.test.desktop.util.AppUtil;
import uz.test.desktop.view.Dialogs;
import uz.test.desktop.view.FxmlView;

import java.net.URL;
import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Test app
 *
 * @author Bakhtiyor
 * @version 0.0.1
 * @since 2021-01-26
 */

@Controller
public class SettingsController implements Initializable {

    @FXML
    private JFXButton btnSettings;

    @FXML
    private Label testSectionsLabel;

    @FXML
    private RadioButton rbSection;

    @FXML
    private RadioButton rbMixed;

    @FXML
    private ToggleGroup tgSection;

    @FXML
    private VBox vbSection;

    @FXML
    private JFXButton btnStartTest;

    @FXML
    private JFXTextField lblSearch;

    @FXML
    private JFXButton btnSearch;

    @FXML
    private JFXButton btnContact;

    @FXML
    private JFXButton btnGoal;

    private List<TreeItem<Question>> selected;
    private List<String> depCodes;
    private Map<String, String> departmentMap;
    private List<String> departmentCodes;
    private FilterData filter;
    private List<QuestionsDto> questionsResultList;
    private List<QuestionsDto> asHideList;
    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private QuestionsService questionsService;

    @Lazy
    @Autowired
    private StageManager stageManager;

    private ResourceBundle resourceBundle;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle = resources;

        filter = new FilterData();
        selected = new ArrayList<>();
        depCodes = new ArrayList<>();
        departmentMap = new HashMap<>();
        departmentCodes = new ArrayList<>();
        questionsResultList = new ArrayList<>();
        asHideList = new ArrayList<>();

        rbMixed.setSelected(true);
        rbMixed.setPadding(new Insets(10D));
        rbMixed.setCursor(Cursor.HAND);

        rbSection.setPadding(new Insets(10D));
        rbSection.setCursor(Cursor.HAND);

        btnStartTest.setPadding(new Insets(10D));

        btnSearch.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                filter.setSearch(lblSearch.getText());
                rbSection.setSelected(Boolean.TRUE);
                loadCategorySettings(Category.SECTION);
            }
        });

        loadCategorySettings(Category.MIXED);
        rbMixed.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (rbMixed.isSelected()) {
                    loadCategorySettings(Category.MIXED);
                }
            }
        });

        rbSection.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (rbSection.isSelected()) {
                    loadCategorySettings(Category.SECTION);
                }
            }
        });
    }

    private void loadCategorySettings(Category category) {

        final VBox root = getSettingsRoot();
        if (root.getChildren().size() > 0) {
            root.getChildren().remove(0);
        }

        boolean isLangChange = false;
        Locale locale = resourceBundle.getLocale();
        if (locale.getLanguage().equalsIgnoreCase("ru")) {
            isLangChange = true;
        }

        switch (category) {
            case MIXED: {
                createMixedRoot(root, isLangChange);
                break;
            }
            case SECTION: {
                createSectionRoot(root, isLangChange);
                break;
            }
        }
    }

    /**
     * Аралаш ойнани чизиш
     */
    private void createMixedRoot(VBox rootM, boolean isLangChange) {
        List<DepartmentDto> departmentList = departmentService.getList();

        departmentMap = departmentList.stream().collect(Collectors.toMap(DepartmentDto::getNameUz, DepartmentDto::getCode));
        depCodes = departmentList.stream().map(DepartmentDto::getNameUz).collect(Collectors.toList());
        if (isLangChange) {
            departmentMap = departmentList.stream().collect(Collectors.toMap(DepartmentDto::getNameRu, DepartmentDto::getCode));
            depCodes = departmentList.stream().map(DepartmentDto::getNameRu).collect(Collectors.toList());
        }

        rootM.getChildren().add(createMixedNode(depCodes, departmentMap));
    }

    /**
     * Аралаш ойна элентлари
     *
     * @param depCodes
     * @param departmentMap
     * @return ListView FX Node
     */
    private ListView<?> createMixedNode(List<String> depCodes, Map<String, String> departmentMap) {

        ObservableList<Department> listSections = FXCollections.observableArrayList(depCodes.stream().map(Department::new).collect(Collectors.toList()));
        ListView<Department> listView = new ListView<>();
        listView.getItems().addAll(listSections);
        listView.setPrefHeight(depCodes.size() * vbSection.getPrefHeight());
        listView.setPrefWidth(depCodes.size() * vbSection.getPrefWidth());

        listSections.forEach(section -> section.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
            if (isNowSelected) {
                departmentCodes.add(departmentMap.get(section.getName()));
            } else {
                departmentCodes.remove(departmentMap.get(section.getName()));
            }
        }));

        listView.setCellFactory(CheckBoxListCell.forListView(Department::selectedProperty, new StringConverter<Department>() {
            @Override
            public String toString(Department object) {
                return object.getName();
            }

            @Override
            public Department fromString(String string) {
                return null;
            }
        }));
        return listView;
    }

    /**
     * Бўлимлар буйича ойна чизиш
     */
    private void createSectionRoot(VBox rootS, boolean isLangChange) {
        rootS.getChildren().add(createSectionNode(getTreeQuestions(filter, isLangChange), isLangChange, filter.getSearch()));
    }

    private VBox getSettingsRoot() {
        return vbSection;
    }

    private List<SimpleTreeItem> getTreeQuestions(FilterData filter, boolean isLangChange) {
        return questionsService.getTreeItemList(filter, isLangChange);
    }

    private TreeView<?> createSectionNode(List<SimpleTreeItem> treeQuestions, boolean isLangChange, String search) {
        CheckBoxTreeItem<Question> rootNode = new CheckBoxTreeItem<>(new Question(resourceBundle.getString("SettingsController.test.sectionRoot"), "ALL"));
        rootNode.setExpanded(true);
        Question nodeDepartment;
        Question nodeQuestion;
        for (SimpleTreeItem question : treeQuestions) {
            nodeQuestion = new Question(isLangChange ? question.getLabelRu() : question.getLabel(), question.getParent().getCode(), question.getId(), Boolean.FALSE);
            CheckBoxTreeItem<Question> quesNode = new CheckBoxTreeItem<>(nodeQuestion);
            boolean found = false;
            for (TreeItem<Question> depNode : rootNode.getChildren()) {
                if (depNode.getValue().getLayerCode().contentEquals(question.getParent().getCode())) {
                    depNode.getChildren().add(quesNode);
                    found = true;
                    break;
                }
            }

            if (!found) {
                nodeDepartment = new Question(isLangChange ? question.getParent().getLabelRu() : question.getParent().getLabel(), question.getParent().getCode(), Boolean.FALSE);
                CheckBoxTreeItem<Question> depNode = new CheckBoxTreeItem<Question>(nodeDepartment);
                rootNode.getChildren().add(depNode);
                depNode.getChildren().add(quesNode);

                if (!AppUtil.isEmpty(search)) {
                    depNode.setExpanded(true);
                }
            }
        }

        rootNode.addEventHandler(CheckBoxTreeItem.checkBoxSelectionChangedEvent(), (CheckBoxTreeItem.TreeModificationEvent<Question> event) -> {
            CheckBoxTreeItem<Question> item = event.getTreeItem();

            if (event.wasIndeterminateChanged()) {
                if (item.isIndeterminate()) {
                    selected.remove(item);
                } else if (item.isSelected()) {
                    selected.add(item);
                }
            } else if (event.wasSelectionChanged()) {
                if (item.isSelected()) {
                    selected.add(item);
                } else {
                    selected.remove(item);
                }
            }
        });

        // listen for subtree add/remove
        rootNode.addEventHandler(TreeItem.childrenModificationEvent(), (TreeItem.TreeModificationEvent<Question> evt) -> {
            if (evt.wasAdded()) {
                for (TreeItem<Question> added : evt.getAddedChildren()) {
                    addSubtree(selected, (CheckBoxTreeItem<Question>) added);
                }
            }
            if (evt.wasRemoved()) {
                for (TreeItem<Question> removed : evt.getRemovedChildren()) {
                    removeSubtree(selected, (CheckBoxTreeItem<Question>) removed);
                }
            }
        });

        final Callback<TreeItem<Question>, ObservableValue<Boolean>> getSelectedProperty =
                (TreeItem<Question> item) -> {
                    if (item instanceof CheckBoxTreeItem<?>) {
                        return ((CheckBoxTreeItem<?>) item).selectedProperty();
                    }
                    return null;
                };

        final StringConverter<TreeItem<Question>> converter =
                new StringConverter<TreeItem<Question>>() {
                    @Override
                    public String toString(TreeItem<Question> object) {
                        Question item = object.getValue();
                        return item.getLayerName();
                    }

                    @Override
                    public TreeItem<Question> fromString(String string) {
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                };

        TreeView<Question> treeView = new TreeView<Question>(rootNode);
        treeView.setEditable(true);
        treeView.setPrefWidth(24.5D * vbSection.getPrefWidth());
        treeView.setPrefHeight(24.5D * vbSection.getPrefHeight());
        treeView.setPadding(new Insets(10D));

        treeView.setCellFactory(item -> {
            CheckBoxTreeCell checkBoxTreeCell = new CheckBoxTreeCell(getSelectedProperty, converter);
            checkBoxTreeCell.prefWidthProperty().bind(treeView.widthProperty().subtract(10.0));

            return checkBoxTreeCell;
        });

        return treeView;
    }

    private static <T> void print(CheckBoxTreeItem<T> item) {
        if (item.isSelected()) {
            System.out.println(item.getValue());
        } else if (!item.isIndeterminate() && !item.isIndependent()) {
            return;
        }
        for (TreeItem<T> child : item.getChildren()) {
            print((CheckBoxTreeItem<T>) child);
        }
    }

    private static <T> void addSubtree(Collection<TreeItem<T>> collection, CheckBoxTreeItem<T> item) {
        if (item.isSelected()) {
            collection.add(item);
        } else if (!item.isIndeterminate() && !item.isIndependent()) {
            return;
        }
        for (TreeItem<T> child : item.getChildren()) {
            addSubtree(collection, (CheckBoxTreeItem<T>) child);
        }
    }

    private static <T> void removeSubtree(Collection<TreeItem<T>> collection, CheckBoxTreeItem<T> item) {
        if (item.isSelected()) {
            collection.remove(item);
        } else if (!item.isIndeterminate() && !item.isIndependent()) {
            return;
        }
        for (TreeItem<T> child : item.getChildren()) {
            removeSubtree(collection, (CheckBoxTreeItem<T>) child);
        }
    }

    @FXML
    void onBtnSettings(ActionEvent event) {
        TabPaneExample.showDialog(stageManager.getPrimaryStage(), "Тақиқ қуйилган саволлар", questionsService, resourceBundle);
    }

    @FXML
    void onBtnStartTest(ActionEvent event) {
        if (rbMixed.isSelected()) {
            setQuestionResult(Category.MIXED);
        } else {
            setQuestionResult(Category.SECTION);
        }
        switchWindow(resourceBundle);
    }

    private void setQuestionResult(Category category) {
        List<String> list = new ArrayList<>(departmentCodes);
        if (category == Category.MIXED) {
            if (list.size() > 3) {
                addRandomisedElement(Arrays.asList(
                        new RandomData(DepartmentType.DEP_CIVIL.getName(),7),
                        new RandomData(DepartmentType.DEP_WORK.getName(),7),
                        new RandomData(DepartmentType.DEP_STOCK_MARKET.getName(),3),
                        new RandomData(DepartmentType.DEP_LIMITED.getName(),3)
                ));

            } else if (list.containsAll(Arrays.asList(DepartmentType.DEP_CIVIL.getName(), DepartmentType.DEP_WORK.getName(), DepartmentType.DEP_STOCK_MARKET.getName()))) {

                addRandomisedElement(Arrays.asList(
                        new RandomData(DepartmentType.DEP_CIVIL.getName(),8),
                        new RandomData(DepartmentType.DEP_WORK.getName(),8),
                        new RandomData(DepartmentType.DEP_STOCK_MARKET.getName(),4)));

                System.out.println("1 :" + departmentCodes);

            } else if (list.containsAll(Arrays.asList(DepartmentType.DEP_CIVIL.getName(), DepartmentType.DEP_WORK.getName(), DepartmentType.DEP_LIMITED.getName()))) {

                addRandomisedElement(Arrays.asList(
                        new RandomData(DepartmentType.DEP_CIVIL.getName(),8),
                        new RandomData(DepartmentType.DEP_WORK.getName(),8),
                        new RandomData(DepartmentType.DEP_LIMITED.getName(),4)));

                System.out.println("2 :" + departmentCodes);

            } else if (list.containsAll(Arrays.asList(DepartmentType.DEP_CIVIL.getName(), DepartmentType.DEP_STOCK_MARKET.getName(), DepartmentType.DEP_LIMITED.getName()))) {

                addRandomisedElement(Arrays.asList(
                        new RandomData(DepartmentType.DEP_CIVIL.getName(),12),
                        new RandomData(DepartmentType.DEP_STOCK_MARKET.getName(),4),
                        new RandomData(DepartmentType.DEP_LIMITED.getName(),4)));

                System.out.println("3 :" + departmentCodes);
            } else if (list.containsAll(Arrays.asList(DepartmentType.DEP_WORK.getName(), DepartmentType.DEP_STOCK_MARKET.getName(), DepartmentType.DEP_LIMITED.getName()))) {

                addRandomisedElement(Arrays.asList(
                        new RandomData(DepartmentType.DEP_WORK.getName(),12),
                        new RandomData(DepartmentType.DEP_STOCK_MARKET.getName(),4),
                        new RandomData(DepartmentType.DEP_LIMITED.getName(),4)));

                System.out.println("4 :" + departmentCodes);
            } else if (list.containsAll(Arrays.asList(DepartmentType.DEP_CIVIL.getName(), DepartmentType.DEP_WORK.getName()))) {

                addRandomisedElement(Arrays.asList(
                        new RandomData(DepartmentType.DEP_CIVIL.getName(),10),
                        new RandomData(DepartmentType.DEP_WORK.getName(),10)));

                System.out.println("5 :" + departmentCodes);

            } else if (list.containsAll(Arrays.asList(DepartmentType.DEP_STOCK_MARKET.getName(), DepartmentType.DEP_LIMITED.getName()))) {
                addRandomisedElement(Arrays.asList(
                        new RandomData(DepartmentType.DEP_STOCK_MARKET.getName(),10),
                        new RandomData(DepartmentType.DEP_LIMITED.getName(),10)));

                System.out.println("6 :" + departmentCodes);
            } else if (list.containsAll(Arrays.asList(DepartmentType.DEP_CIVIL.getName(), DepartmentType.DEP_STOCK_MARKET.getName()))) {

                addRandomisedElement(Arrays.asList(
                        new RandomData(DepartmentType.DEP_CIVIL.getName(),14),
                        new RandomData(DepartmentType.DEP_STOCK_MARKET.getName(),6)));

                System.out.println("7 :" + departmentCodes);
            } else if (list.containsAll(Arrays.asList(DepartmentType.DEP_CIVIL.getName(), DepartmentType.DEP_LIMITED.getName()))) {

                addRandomisedElement(Arrays.asList(
                        new RandomData(DepartmentType.DEP_CIVIL.getName(),14),
                        new RandomData(DepartmentType.DEP_LIMITED.getName(),6)));

                System.out.println("8 :" + departmentCodes);
            } else if (list.containsAll(Arrays.asList(DepartmentType.DEP_WORK.getName(), DepartmentType.DEP_STOCK_MARKET.getName()))) {

                addRandomisedElement(Arrays.asList(
                        new RandomData(DepartmentType.DEP_WORK.getName(),14),
                        new RandomData(DepartmentType.DEP_STOCK_MARKET.getName(),6)));

                System.out.println("9 :" + departmentCodes);

            }  else if (list.containsAll(Arrays.asList(DepartmentType.DEP_WORK.getName(), DepartmentType.DEP_LIMITED.getName()))) {

                addRandomisedElement(Arrays.asList(
                        new RandomData(DepartmentType.DEP_WORK.getName(), 14),
                        new RandomData(DepartmentType.DEP_LIMITED.getName(), 6)));

                System.out.println("10 :" + departmentCodes);
            } else if (list.size() > 0){

                addRandomisedElement(Collections.singletonList(new RandomData(list.get(0), 20)));
                System.out.println("11 :" + departmentCodes);
            }

        } else {
            List<Long> copy = new ArrayList<>();
            for (TreeItem treeItem : selected) {
                Question question = (Question) treeItem.getValue();
                copy.add(question.getLayerId());
            }
            addSelectedQuestions(copy);
        }
    }

    private void addRandomisedElement(List<RandomData> types) {
        for (RandomData element : types) {
            List<QuestionsDto> questions = questionsService.findByDepartmentCode(element.getType(), element.getLimit());
            List<QuestionsDto> copy = new ArrayList<>(questions);

            SecureRandom rand = new SecureRandom();
            for (int i = 0; i < Math.min(element.getLimit(), questions.size()); i++) {
                questionsResultList.add(copy.remove(rand.nextInt(copy.size())));
            }
        }
    }

    private void addSelectedQuestions(List<Long> ids) {
        List<QuestionsDto> questions = questionsService.findBySelectedQuestion(ids);
        questionsResultList.addAll(questions);
        //System.out.println(questionsResultList);
    }

    private void switchWindow(ResourceBundle bundle) {
        if (questionsResultList.isEmpty()) {
            Dialogs.showWarningDialog(stageManager.getPrimaryStage(),
                    resourceBundle.getString("Alert.dialog.warning.content"),
                    resourceBundle.getString("Alert.dialog.warning.header"));
            return;
        }
        stageManager.switchScene(FxmlView.ACTION, bundle);
    }

    @FXML
    void onBtnGoal(ActionEvent event) {
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.initStyle(StageStyle.UNDECORATED);
        a.setGraphic(null);
        a.setTitle("Alert Title");
        a.setHeaderText(null);
        a.setResizable(true);
        WebView webView = new WebView();
        webView.getEngine().loadContent(resourceBundle.getString("SettingsController.test.contact.goal"));
        webView.setPrefSize(450, 450);
        a.getDialogPane().setContent(webView);
        a.showAndWait();
    }

    @FXML
    void onBtnContact(ActionEvent event) {
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.initStyle(StageStyle.UNDECORATED);
        a.setGraphic(null);
        a.setTitle("Alert Title");
        a.setHeaderText(null);
        a.setResizable(true);
        WebView webView = new WebView();
        webView.getEngine().loadContent(resourceBundle.getString("SettingsController.test.contact.requisites"));
        webView.setPrefSize(450, 450);
        a.getDialogPane().setContent(webView);
        a.showAndWait();
    }

    public ObservableList<Questions> createSectionNode() {
        ObservableList<Questions> data = FXCollections.observableArrayList();
        List<QuestionsDto> questions = questionsService.findByHiddenQuestions();

        boolean isLangChange = false;
        Locale locale = resourceBundle.getLocale();
        if (locale.getLanguage().equalsIgnoreCase("ru")) {
            isLangChange = true;
        }
        for (QuestionsDto questionsDto : questions) {
            data.add(new Questions(questionsDto.getId(), isLangChange ? questionsDto.getQuestionRu() : questionsDto.getQuestionUz()));
        }
        return data;
    }


    public static class Question {

        private String layerName;
        private String layerCode;
        private Long layerId;
        private boolean selected;

        public Question(String layerCode, boolean selected) {
            this.layerCode = layerCode;
            this.selected = selected;
        }

        public Question(String layerName, String layerCode, boolean selected) {
            this.layerName = layerName;
            this.layerCode = layerCode;
            this.selected = selected;
        }

        public Question(String layerName, String layerCode) {
            this.layerName = layerName;
            this.layerCode = layerCode;
        }

        public Question(String layerName, String layerCode, Long layerId, boolean selected) {
            this.layerName = layerName;
            this.layerCode = layerCode;
            this.layerId = layerId;
            this.selected = selected;
        }

        public String getLayerName() {
            return layerName;
        }

        public void setLayerName(String layerName) {
            this.layerName = layerName;
        }

        public Long getLayerId() {
            return layerId;
        }

        public void setLayerId(Long layerId) {
            this.layerId = layerId;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public String getLayerCode() {
            return layerCode;
        }

        public void setLayerCode(String layerCode) {
            this.layerCode = layerCode;
        }

        @Override
        public String toString() {
            return "Question{" +
                    "layerName='" + layerName + '\'' +
                    ", layerId=" + layerId +
                    ", layerCode=" + layerCode +
                    '}';
        }
    }

    public static class Department {
        private ReadOnlyStringWrapper name = new ReadOnlyStringWrapper();
        private BooleanProperty selected = new SimpleBooleanProperty(false);

        public Department(String name) {
            this.name.set(name);
        }

        public String getName() {
            return name.get();
        }

        public ReadOnlyStringProperty nameProperty() {
            return name.getReadOnlyProperty();
        }

        public BooleanProperty selectedProperty() {
            return selected;
        }

        public boolean isSelected() {
            return selected.get();
        }

        public void setSelected(boolean selected) {
            this.selected.set(selected);
        }
    }

    public List<QuestionsDto> getQuestionsResultList() {
        return questionsResultList;
    }

    public void setQuestionsResultList(List<QuestionsDto> questionsResultList) {
        this.questionsResultList = questionsResultList;
    }

    public List<QuestionsDto> getAsHideList() {
        return asHideList;
    }

    public void setAsHideList(List<QuestionsDto> asHideList) {
        this.asHideList = asHideList;
    }

    static class ConfigModal extends Stage {

        public ConfigModal(String title,Stage stage, Scene scene) {
            setTitle(title);
            initStyle(StageStyle.UTILITY);
            initModality(Modality.APPLICATION_MODAL);
            initOwner(stage);
            setResizable(false);
            setScene(scene);
        }

        public void showDialog() {
            sizeToScene();
            centerOnScreen();
            showAndWait();
        }
    }


}
