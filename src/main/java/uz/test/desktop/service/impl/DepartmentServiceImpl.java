package uz.test.desktop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.test.desktop.domain.DepartmentEntity;
import uz.test.desktop.dto.DepartmentDto;
import uz.test.desktop.dto.common.SimpleTree;
import uz.test.desktop.repository.DepartmentRepository;
import uz.test.desktop.service.DepartmentService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public List<DepartmentEntity> findAllBySortCode() {
        return departmentRepository.getBySortCodeList();
    }

    @Override
    @Transactional(readOnly = true)
    public List<DepartmentDto> getList() {
        List<DepartmentEntity> entDepartmentList = departmentRepository.findAll();
        return entDepartmentList.stream().map(DepartmentEntity::asDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<SimpleTree> getTreeList() {
        List<DepartmentEntity> entDepartmentList = departmentRepository.findAll();
        return entDepartmentList.stream().map(DepartmentEntity::asTreeDto).collect(Collectors.toList());
    }
}
