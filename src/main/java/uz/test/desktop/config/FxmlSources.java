package uz.test.desktop.config;

public abstract class FxmlSources {

    public static final String CONTROLS_VIEW = "/controls-view.fxml";
    public static final String SETTINGS_VIEW = "/settings-view-old.fxml";
}
