package uz.test.desktop.controller;

import com.jfoenix.controls.JFXButton;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import uz.test.desktop.config.StageManager;
import uz.test.desktop.domain.UserEntity;
import uz.test.desktop.dto.QuestionsDto;
import uz.test.desktop.repository.DepartmentRepository;
import uz.test.desktop.repository.UserRepository;
import uz.test.desktop.service.DepartmentService;
import uz.test.desktop.service.QuestionsService;
import uz.test.desktop.service.UserService;
import uz.test.desktop.util.ExcelReader;
import uz.test.desktop.view.FxmlView;


import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@Controller
public class ControlsController implements Initializable {

    private static final Logger LOG = LoggerFactory.getLogger(ControlsController.class);
    private static final String BUNDLE_NAME = "messages/messages";

    @FXML
    private JFXButton btnEnter;

    @FXML
    private JFXButton btnRu;

    @FXML
    private JFXButton btnUz;

    @FXML
    private JFXButton btnExit;

    @FXML
    private JFXButton btnHelp;

    @FXML
    private VBox vBoxBtn;

    private SettingsController settingsController;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuestionsService questionsService;

    @Autowired
    private DepartmentService departmentService;

    @Lazy
    @Autowired
    private StageManager stageManager;

    private ResourceBundle resourceBundle;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resourceBundle = resources;
        btnRu.setOnMouseClicked(mouseEvent ->  {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                if (mouseEvent.getClickCount() == 2) {
                    switchLanguage(new Locale("ru", "RU"));
                    loadWindow(resourceBundle);
                }
            }
        });

        btnUz.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                if (mouseEvent.getClickCount() == 2) {
                    switchLanguage(new Locale("uz", "UZ"));
                    loadWindow(resourceBundle);
                }
            }
        });
    }

    @FXML
    void onBtnHelp(ActionEvent event) {
        loadWeatherForecast();
    }

    @FXML
    void onBtnExit(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void onBtnUz(ActionEvent event) {
        switchLanguage(new Locale("uz", "UZ"));
    }

    @FXML
    void onBtnRu(ActionEvent event) {
        switchLanguage(new Locale("ru", "RU"));
    }

    @FXML
    void onEnter(ActionEvent event) throws IOException {
        loadWindow(resourceBundle);
    }

    private void loadWindow(ResourceBundle bundle) {
        stageManager.switchScene(FxmlView.SETTINGS,bundle);
    }

    private void switchLanguage(Locale locale) {
        resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME, locale);
    }

    public void loadWeatherForecast() {

        departmentService.findAllBySortCode();
        questionsService.findByDepartmentId(2l);
        try {
            List<QuestionsDto> questionsList = ExcelReader.getExcel();
            questionsService.save(questionsList);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        userService.save();
        List<UserEntity> userEntityList = userRepository.findAll();

        LOG.info("Users: {}", userEntityList);

    }

}
