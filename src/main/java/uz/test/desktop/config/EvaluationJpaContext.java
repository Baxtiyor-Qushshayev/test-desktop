package uz.test.desktop.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by Dilsh0d on 08.08.2017.
 */
@Configuration
@EntityScan(basePackages = {"uz.test.desktop.domain"})
@EnableJpaRepositories(basePackages = "uz.test.desktop.repository")
//@EnableTransactionManagement
public class EvaluationJpaContext {

}
