package uz.test.desktop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.test.desktop.domain.DepartmentEntity;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<DepartmentEntity, Long> {

    @Query(value = "SELECT * FROM DEPARTMENT D " +
            "ORDER BY D.CODE ASC ", nativeQuery = true)
    List<DepartmentEntity> getBySortCodeList();

}
