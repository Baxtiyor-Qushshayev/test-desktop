package uz.test.desktop.domain;


import org.hibernate.annotations.Type;
import uz.test.desktop.domain.base.BaseEntity;
import uz.test.desktop.dto.AnswersDto;

import javax.persistence.*;

@Entity
@Table(name = "answers")
public class AnswersEntity extends BaseEntity {

    @Column(name = "answerUz")
    @Type(type = "text")
    private String answerUz;

    @Column(name = "answerRu")
    @Type(type = "text")
    private String answerRu;

    @Column(name="isAnswer", columnDefinition = "boolean default false")
    private Boolean isAnswer = Boolean.FALSE;

    @Column(name = "questionId")
    private Long questionId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "questionId", insertable = false, updatable = false)
    private QuestionsEntity question;


    public String getAnswerUz() {
        return answerUz;
    }

    public void setAnswerUz(String answerUz) {
        this.answerUz = answerUz;
    }

    public String getAnswerRu() {
        return answerRu;
    }

    public void setAnswerRu(String answerRu) {
        this.answerRu = answerRu;
    }

    public Boolean getAnswer() {
        return isAnswer;
    }

    public void setAnswer(Boolean answer) {
        isAnswer = answer;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public QuestionsEntity getQuestion() {
        return question;
    }

    public void setQuestion(QuestionsEntity question) {
        this.question = question;
    }

    public AnswersDto asDto() {
        AnswersDto dto = new AnswersDto();
        dto.setId(getId());
        dto.setAnswerUz(getAnswerUz());
        dto.setAnswerRu(getAnswerRu());
        dto.setAnswer(getAnswer());

        return dto;
    }
}
